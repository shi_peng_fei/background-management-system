#!/bin/bash

echo "进入到对应的路径"
cd ../Niaofei
echo "目前的路径"
pwd

echo "停止容器"
docker stop niaofeiweb

echo "删除容器"
docker rm niaofeiweb

echo "删除镜像"
docker rmi niaofeiweb:1.0

docker build -t niaofeiweb:1.0 .

docker run -d --name=niaofeiweb -p 10086:80 niaofeiweb:1.0

echo "后端发布成功"
