﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Repository
{
    public interface IDbContextProvider<TDbContext> where TDbContext : DbContext
    {
        Task<TDbContext> GetDbContextAsync();
    }
}
