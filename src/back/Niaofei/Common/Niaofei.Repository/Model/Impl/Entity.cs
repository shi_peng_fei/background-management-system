﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Repository
{
    public abstract class Entity<TKey> : IEntity<TKey>
    {
        public Entity()
        {

        }

        public Entity(TKey key)
        {
            Id = key;
        }

        [Key]
        public virtual TKey Id { get; protected set; }

        public object[] GetKeys()
        {
            return new object[1] { this.Id };
        }
    }

    public abstract class Entity : Entity<int>
    {

    }
}
