﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Repository
{
    public interface ISoftDelete
    {
        /// <summary>
        /// 删除标识
        /// </summary>
        bool IsDeleted { get; set; }
    }
}
