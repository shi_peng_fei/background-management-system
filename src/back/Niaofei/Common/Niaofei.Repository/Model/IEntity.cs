﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Repository
{
    public interface IEntity<TKey> : IEntity
    {
        TKey Id { get; }
    }

    public interface IEntity
    {
        object[] GetKeys();
    }
}
