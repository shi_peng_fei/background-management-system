﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Repository
{
    public interface IDataBaseAutoSave
    {
        /// <summary>
        /// 自动保存
        /// </summary>
        /// <returns></returns>
        Task SaveChangesAsync();
    }
}
