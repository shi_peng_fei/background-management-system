﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application
{
    public static class ProfilesExtensions
    {
        /// <summary>
        /// AutoMapper映射自动添加
        /// </summary>
        public static void AddAutoMapperServices(this IServiceCollection services)
        {
            services.AddAutoMapper(m => m.AddMaps(new List<Assembly>() { typeof(SysDictProfile).Assembly}));
        }
    }
}
