﻿using Niaofei.Application.Contracts;
using Niaofei.Domain;
using Niaofei.Injcetion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application
{
    [Injection(typeof(ITestAppService))]

    internal class TestAppService : ITestAppService
    {
        private readonly ITestRepository _testRepository;

        public TestAppService(ITestRepository testRepository)
        {
            _testRepository = testRepository;
        }

        public Task Test()
        {
            return _testRepository.Test();
        }
    }
}
