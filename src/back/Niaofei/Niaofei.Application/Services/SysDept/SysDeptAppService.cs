﻿using Microsoft.Extensions.DependencyInjection;
using Niaofei.Application.Contracts;
using Niaofei.Domain;
using Niaofei.Injcetion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application
{
    [Injection(typeof(ISysDeptAppService))]
    internal class SysDeptAppService : NiaofeiAppService, ISysDeptAppService
    {
        private readonly ISysDeptRepository _sysDeptRepository;

        public SysDeptAppService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _sysDeptRepository = serviceProvider.GetRequiredService<ISysDeptRepository>();
        }

        public async Task<ResultDto<SysDeptDto>> AddSysDept(SysDeptCreateDto input)
        {
            var entity = Mapper.Map<SysDeptCreateDto, SysDept>(input);
            entity.IsDeleted = false;
            entity.CreateTime = DateTime.Now;
            entity.UserId = CurrentUser?.Id;

            entity = await _sysDeptRepository.InsertAsync(entity);

            var result = Mapper.Map<SysDept, SysDeptDto>(entity);

            return new ResultDto<SysDeptDto>(result);
        }

        public async Task<ResultDto<List<SysDeptTreeDto>>> GetSysDept(SysDeptInputDto input)
        {
            var entitys = (await _sysDeptRepository.GetQueryableAsync())
                .Where(m => !m.IsDeleted)
                .ToList();

            var depts = Mapper.Map<List<SysDept>, List<SysDeptTreeDto>>(entitys);

            var result = depts.Where(m => m.ParentId == null).ToList();

            foreach (var item in result)
            {
                item.TreePath = item.Name;
                item.SetChiren(depts);
            }

            return new ResultDto<List<SysDeptTreeDto>>(result);
        }

        public async Task<ResultDto> DeleteSysDept(List<long> ids)
        {
            var entitys = (await _sysDeptRepository.GetQueryableAsync())
                .Where(m => ids.Contains(m.Id))
                .Where(m => !m.IsDeleted)
                .ToList();

            await _sysDeptRepository.DeleteManyAsync(entitys);

            return new ResultDto() { Code = "0", Data = "成功" };
        }

        public async Task<ResultDto<SysDeptDto>> UpdateSysDept(SysDeptUpdateDto input)
        {
            var entity = (await _sysDeptRepository.GetQueryableAsync())
                .FirstOrDefault(m => m.Id == input.Id && !m.IsDeleted);

            if (entity == null)
            {
                throw new Exception("数据不存在");
            }

            Mapper.Map(input, entity);

            if (entity.ParentId != null && entity.ParentId != 0)
            {
                var pEntity = (await _sysDeptRepository.GetQueryableAsync()).First(m => m.Id == entity.ParentId);

                entity.TreePath = pEntity.TreePath + $",{entity.Id}";
            }
            else
            {
                entity.TreePath = entity.Id.ToString();
            }

            entity = await _sysDeptRepository.UpdateAsync(entity);

            var result = Mapper.Map<SysDept, SysDeptDto>(entity);

            return new ResultDto<SysDeptDto>(result);
        }
    }
}
