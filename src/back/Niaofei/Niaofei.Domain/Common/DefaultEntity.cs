﻿using Niaofei.Repository;

namespace Niaofei.Domain.Common
{
    public class DefaultEntity<TKey> : Entity<TKey>
    {
        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }

        public string? UserId { get; set; }
    }
}
