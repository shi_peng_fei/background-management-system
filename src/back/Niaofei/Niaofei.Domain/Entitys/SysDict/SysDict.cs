﻿using Niaofei.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Domain
{
    /// <summary>
    /// 数据字典表
    /// </summary>
    [Table("sys_dict")]
    public class SysDict : DefaultEntity<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; protected set; }

        /// <summary>
        /// 数据字典类型Id
        /// </summary>
        public int SysDictTypeId { get; set; }

        /// <summary>
        /// 字典类型编码
        /// </summary>
        public string TypeCode { get; set; }

        /// <summary>
        /// 字典项名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 字段项值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态：启用-1，禁用-0
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
