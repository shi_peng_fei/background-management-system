﻿using Niaofei.Repository;

namespace Niaofei.Domain
{
    public interface ISysDeptRepository : IRepository<SysDept>
    {
    }
}
