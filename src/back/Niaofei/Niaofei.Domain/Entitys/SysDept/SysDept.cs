﻿using Niaofei.Domain.Common;
using Niaofei.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Domain
{
    /// <summary>
    /// 部门信息表
    /// </summary>
    [Table("sys_dept")]
    public class SysDept : DefaultEntity<int>, ISoftDelete
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 上级部门
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// 部门路径
        /// </summary>
        public string? TreePath { get; set; }

        /// <summary>
        /// 显示顺序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态：0-正常, 1-禁用
        /// </summary>
        public int Status { get; set; }

        public bool IsDeleted { get; set; }
    }
}
