﻿using Niaofei.Repository;

namespace Niaofei.Domain
{
    public interface ISysMenuRepository : IRepository<SysMenu>
    {
    }
}
