﻿using Niaofei.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Domain
{
    /// <summary>
    /// 权限管理
    /// </summary>
    [Table("sys_rights")]
    public class SysRights : Entity<int>
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public int RoleId { get; set; }
       
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int MenuId { get; set; }

        /// <summary>
        /// 权限标识
        /// </summary>
        public string Rights { get; set; }
    }
}
