﻿using Niaofei.Domain.Common;
using Niaofei.Repository;
using System.ComponentModel.DataAnnotations.Schema;

namespace Niaofei.Domain
{
    /// <summary>
    /// 用户信息表
    /// </summary>
    [Table("sys_user")]
    public class SysUser : DefaultEntity<long>, ISoftDelete
    {
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string PassWord { get; set; }

        /// <summary>
        /// 部门Id
        /// </summary>
        public int DeptId { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 状态：0-正常, 1-禁用
        /// </summary>
        public int Status { get; set; }

        public bool IsDeleted { get; set; }
    }
}
