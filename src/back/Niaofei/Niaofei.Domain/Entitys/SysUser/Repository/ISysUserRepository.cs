﻿using Niaofei.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Domain
{
    public interface ISysUserRepository : IRepository<SysUser>
    {
    }
}
