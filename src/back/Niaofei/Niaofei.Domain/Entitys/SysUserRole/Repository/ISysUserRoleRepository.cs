﻿using Niaofei.Repository;

namespace Niaofei.Domain
{
    public interface ISysUserRoleRepository : IRepository<SysUserRole>
    {
    }
}
