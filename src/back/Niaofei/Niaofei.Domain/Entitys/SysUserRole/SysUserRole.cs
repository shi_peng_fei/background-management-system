﻿using Niaofei.Repository;
using System.ComponentModel.DataAnnotations.Schema;

namespace Niaofei.Domain
{
    /// <summary>
    /// 用户角色关联表
    /// </summary>
    [Table("sys_user_role")]
    public class SysUserRole : Entity<int>
    {
        public long UserId { get; set; }

        public int RoleId { get; set; }
    }
}
