﻿using Niaofei.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Domain
{
    public class Test : Entity<Guid>
    {
        /// <summary>
        /// 自增
        /// </summary>
        public int Value { get; set; }
    }
}
