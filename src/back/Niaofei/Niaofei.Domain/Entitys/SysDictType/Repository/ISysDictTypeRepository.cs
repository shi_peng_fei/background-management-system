﻿using Niaofei.Repository;

namespace Niaofei.Domain
{
    public interface ISysDictTypeRepository : IRepository<SysDictType>
    {
    }
}
