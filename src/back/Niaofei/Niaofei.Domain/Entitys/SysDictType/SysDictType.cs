﻿using Niaofei.Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Niaofei.Domain
{
    /// <summary>
    /// 字典类型表
    /// </summary>
    [Table("sys_dict_type")]
    public class SysDictType : DefaultEntity<int>
    {
        /// <summary>
        /// 类型编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 类型名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态：0-正常，1-禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
