﻿using Niaofei.Repository;

namespace Niaofei.Domain
{
    public interface ISysRoleRepository : IRepository<SysRole>
    {
    }
}
