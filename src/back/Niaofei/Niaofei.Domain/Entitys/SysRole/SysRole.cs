﻿using Niaofei.Repository;
using System.ComponentModel.DataAnnotations.Schema;

namespace Niaofei.Domain
{
    /// <summary>
    /// 角色表
    /// </summary>
    [Table("sys_role")]
    public class SysRole : Entity<int>
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 角色编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 角色状态：正常-1, 停用-0
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 权限数据
        /// </summary>
        public int DataScope { get; set; }
    }
}
