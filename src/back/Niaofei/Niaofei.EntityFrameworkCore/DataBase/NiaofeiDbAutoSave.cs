﻿using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(IDataBaseAutoSave))]
    internal class NiaofeiDbAutoSave : IDataBaseAutoSave
    {
        private readonly IDbContextProvider<NiaofeiDbContext> _dbContextProvider;

        public NiaofeiDbAutoSave(IDbContextProvider<NiaofeiDbContext> dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public async Task SaveChangesAsync()
        {
            var db = await _dbContextProvider.GetDbContextAsync();

            if (db.ChangeTracker.HasChanges())
            {
                await db.SaveChangesAsync();
            }
        }
    }
}
