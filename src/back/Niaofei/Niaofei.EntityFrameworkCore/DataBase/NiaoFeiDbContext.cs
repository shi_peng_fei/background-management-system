﻿using Microsoft.EntityFrameworkCore;
using Niaofei.Domain;

namespace Niaofei.EntityFrameworkCore
{
    public class NiaofeiDbContext : DbContext
    {
        public NiaofeiDbContext(DbContextOptions<NiaofeiDbContext> options) : base(options)
        {

        }

        public DbSet<SysUser> SysUser { get; set; }

        public DbSet<SysDept> SysDept { get; set; }

        public DbSet<SysRole> SysRole { get; set; }

        public DbSet<SysUserRole> SysUserRole { get; set; }

        public DbSet<SysDict> SysDict { get; set; }

        public DbSet<SysDictType> SysDictType { get; set; }

        public DbSet<SysMenu> SysMenu { get; set; }

        public DbSet<Test> Tests { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);

            base.OnModelCreating(modelBuilder);
        }
    }
}
