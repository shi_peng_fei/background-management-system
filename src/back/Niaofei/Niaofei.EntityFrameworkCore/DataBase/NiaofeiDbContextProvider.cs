﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(IDbContextProvider<NiaofeiDbContext>))]
    internal class NiaofeiDbContextProvider : IDbContextProvider<NiaofeiDbContext>
    {
        private readonly IServiceProvider _serviceProvider;

        public NiaofeiDbContextProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task<NiaofeiDbContext> GetDbContextAsync()
        {
            var dbContext = _serviceProvider.GetRequiredService<NiaofeiDbContext>();

            return Task.FromResult(dbContext);
        }
    }
}
