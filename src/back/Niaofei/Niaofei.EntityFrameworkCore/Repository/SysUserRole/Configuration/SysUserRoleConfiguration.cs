﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Niaofei.Domain;

namespace Niaofei.EntityFrameworkCore
{
    internal class SysUserRoleConfiguration : IEntityTypeConfiguration<SysUserRole>
    {
        public void Configure(EntityTypeBuilder<SysUserRole> builder)
        {
            builder.Property(m => m.UserId).HasComment("用户Id");
            builder.Property(m => m.RoleId).HasComment("角色Id");
        }
    }
}
