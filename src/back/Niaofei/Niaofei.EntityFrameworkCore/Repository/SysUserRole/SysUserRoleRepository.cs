﻿using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ISysUserRoleRepository))]
    internal class SysUserRoleRepository : EFCoreRepository<NiaofeiDbContext, SysUserRole>, ISysUserRoleRepository
    {
        public SysUserRoleRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
