﻿using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ISysMenuRepository))]
    internal class SysMenuRepository : EFCoreRepository<NiaofeiDbContext, SysMenu>, ISysMenuRepository
    {
        public SysMenuRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
