﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Niaofei.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.EntityFrameworkCore
{
    internal class SysMenuConfiguration : IEntityTypeConfiguration<SysMenu>
    {
        public void Configure(EntityTypeBuilder<SysMenu> builder)
        {
            builder.Property(m => m.ParentId).HasComment("父级菜单Id");
            builder.Property(m => m.Name).HasMaxLength(255).HasComment("菜单名称");
            builder.Property(m => m.Type).HasMaxLength(255).HasComment("菜单类型");
            builder.Property(m => m.Path).HasMaxLength(1000).HasComment("浏览器地址");
            builder.Property(m => m.Component).HasMaxLength(255).HasComment("组件名称");
            builder.Property(m => m.Visible).HasMaxLength(255).HasComment("显示标识");
            builder.Property(m => m.Sort).HasComment("排序");
            builder.Property(m => m.Icon).HasMaxLength(255).HasComment("图标");
            builder.Property(m => m.RedirectUrl).HasMaxLength(500).HasComment("外链接路径");
            builder.Property(m => m.Redirect).HasMaxLength(500).HasComment("跳转路径");
        }
    }
}
