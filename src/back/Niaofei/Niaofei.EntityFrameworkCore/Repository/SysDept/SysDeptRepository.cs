﻿using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ISysDeptRepository))]
    internal class SysDeptRepository : EFCoreRepository<NiaofeiDbContext, SysDept>, ISysDeptRepository
    {
        public SysDeptRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
