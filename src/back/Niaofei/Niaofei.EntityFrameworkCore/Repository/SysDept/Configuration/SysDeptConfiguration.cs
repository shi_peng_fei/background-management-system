﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Niaofei.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.EntityFrameworkCore
{
    internal class SysDeptConfiguration : IEntityTypeConfiguration<SysDept>
    {
        public void Configure(EntityTypeBuilder<SysDept> builder)
        {
            builder.Property(m => m.Name).HasMaxLength(255).HasComment("部门名称");
            builder.Property(m => m.ParentId).HasComment("上级部门");
            builder.Property(m => m.TreePath).HasMaxLength(500).HasComment("部门路径");
            builder.Property(m => m.Sort).HasComment("排序");
            builder.Property(m => m.Status).HasComment(" 状态：0-正常, 1-禁用");
        }
    }
}
