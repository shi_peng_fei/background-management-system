﻿using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ISysDictTypeRepository))]
    internal class SysDictTypeRepository : EFCoreRepository<NiaofeiDbContext, SysDictType>, ISysDictTypeRepository
    {
        public SysDictTypeRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
