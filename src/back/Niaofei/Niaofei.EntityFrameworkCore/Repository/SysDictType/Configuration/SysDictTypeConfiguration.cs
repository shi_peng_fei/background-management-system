﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Niaofei.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.EntityFrameworkCore
{
    internal class SysDictTypeConfiguration : IEntityTypeConfiguration<SysDictType>
    {
        public void Configure(EntityTypeBuilder<SysDictType> builder)
        {
            builder.Property(m => m.Code).HasMaxLength(256).HasComment("类型编码");
            builder.Property(m => m.Name).HasMaxLength(256).HasComment("类型名称");
            builder.Property(m => m.Status).HasComment("状态：0-正常，1-禁用");
            builder.Property(m => m.Remark).HasMaxLength(2000).HasComment("备注");
        }
    }
}
