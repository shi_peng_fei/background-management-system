﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Niaofei.Domain;

namespace Niaofei.EntityFrameworkCore
{
    internal class SysRoleConfiguration : IEntityTypeConfiguration<SysRole>
    {
        public void Configure(EntityTypeBuilder<SysRole> builder)
        {
            builder.Property(m => m.Name).HasMaxLength(255).HasComment("角色名称");
            builder.Property(m => m.Code).HasMaxLength(255).HasComment("角色编码");
            builder.Property(m => m.Sort).HasComment("排序");
            builder.Property(m => m.Status).HasComment("角色状态：正常-1, 停用-0");
            builder.Property(m => m.DataScope).HasComment("数据权限");
        }
    }
}
