﻿    using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ISysRoleRepository))]
    public class SysRoleRepository : EFCoreRepository<NiaofeiDbContext, SysRole>, ISysRoleRepository
    {
        public SysRoleRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
