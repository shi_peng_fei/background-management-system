﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Niaofei.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.EntityFrameworkCore
{
    internal class SysRightsConfiguration : IEntityTypeConfiguration<SysRights>
    {
        public void Configure(EntityTypeBuilder<SysRights> builder)
        {
            builder.Property(m => m.RoleId).HasComment("角色Id");
            builder.Property(m => m.MenuId).HasComment("菜单Id");
            builder.Property(m => m.Rights).HasMaxLength(1000).HasComment("权限标识");
        }
    }
}
