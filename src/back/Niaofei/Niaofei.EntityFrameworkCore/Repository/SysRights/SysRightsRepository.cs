﻿using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ISysMenuRepository))]
    internal class SysRightsRepository : EFCoreRepository<NiaofeiDbContext, SysMenu>, ISysMenuRepository
    {
        public SysRightsRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
