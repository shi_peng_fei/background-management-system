﻿using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ISysUserRepository))]
    internal class SysUserRepository : EFCoreRepository<NiaofeiDbContext, SysUser>, ISysUserRepository
    {
        public SysUserRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
