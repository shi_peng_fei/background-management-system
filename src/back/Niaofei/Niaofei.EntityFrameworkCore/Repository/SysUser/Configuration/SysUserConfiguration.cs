﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Niaofei.Domain;

namespace Niaofei.EntityFrameworkCore
{
    internal class SysUserConfiguration : IEntityTypeConfiguration<SysUser>
    {
        public SysUserConfiguration()
        {
        }

        public void Configure(EntityTypeBuilder<SysUser> builder)
        {
            builder.Property(m => m.UserName).HasMaxLength(100).HasComment("用户姓名");
            builder.Property(m => m.NickName).HasMaxLength(100).HasComment("用户名称");
            builder.Property(m => m.PassWord).HasMaxLength(100).HasComment("密码");
            builder.Property(m => m.Avatar).HasMaxLength(200).HasComment("头像");
            builder.Property(m => m.Mobile).HasMaxLength(100).HasComment("联系方式");
            builder.Property(m => m.Email).HasMaxLength(100).HasComment("邮箱");
            builder.Property(m => m.Age).HasComment("年龄");
            builder.Property(m => m.DeptId).HasComment("部门Id");
            builder.Property(m => m.Status).HasComment("状态：0-正常, 1-禁用");
        }
    }
}
