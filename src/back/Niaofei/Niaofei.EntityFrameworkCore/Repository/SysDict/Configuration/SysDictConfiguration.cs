﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Niaofei.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.EntityFrameworkCore
{
    internal class SysDictConfiguration : IEntityTypeConfiguration<SysDict>
    {
        public void Configure(EntityTypeBuilder<SysDict> builder)
        {
            builder.Property(m => m.TypeCode).HasMaxLength(100).HasComment("字典类型编码");
            builder.Property(m => m.Name).HasMaxLength(100).HasComment("字典项名称");
            builder.Property(m => m.Value).HasMaxLength(1000).HasComment("字典项值");
            builder.Property(m => m.Sort).HasComment("排序");
            builder.Property(m => m.Value).HasComment("状态：启用-1，禁用-0");
            builder.Property(m => m.IsDefault).HasComment("是否默认");
            builder.Property(m => m.Remark).HasMaxLength(500).HasComment("备注");
        }
    }
}
