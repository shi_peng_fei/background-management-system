﻿using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ISysDictRepository))]
    internal class SysDictRepository : EFCoreRepository<NiaofeiDbContext, SysDict>, ISysDictRepository
    {
        public SysDictRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
