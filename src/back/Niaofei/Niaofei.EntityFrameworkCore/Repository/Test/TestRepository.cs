﻿using Microsoft.EntityFrameworkCore;
using Niaofei.Domain;
using Niaofei.Injcetion;
using Niaofei.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.EntityFrameworkCore
{
    [Injection(typeof(ITestRepository))]
    internal class TestRepository : EFCoreRepository<NiaofeiDbContext, Test>, ITestRepository
    {
        private readonly IDbContextProvider<NiaofeiDbContext> _dbContextProvider;

        public TestRepository(IDbContextProvider<NiaofeiDbContext> dbContextProvider) : base(dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        public async Task Test()
        {
            var db = await _dbContextProvider.GetDbContextAsync();

            var item = new Test();
            db.Entry(item).State = EntityState.Added;

            await db.SaveChangesAsync();

            db.Entry(item).State = EntityState.Detached;

            var a = db.Tests.FirstOrDefault(m => m.Id == item.Id);
        }
    }
}
