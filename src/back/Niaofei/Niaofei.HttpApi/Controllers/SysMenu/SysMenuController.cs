﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Niaofei.Application.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.HttpApi
{
    /// <summary>
    /// 菜单栏
    /// </summary>
    [Authorize]
    [Route("api/sys-menu")]
    public class SysMenuController : ControllerBase
    {
        private readonly ISysMenuAppService _sysMenuAppService;

        public SysMenuController(IServiceProvider serviceProvider)
        {
            _sysMenuAppService = serviceProvider.GetRequiredService<ISysMenuAppService>();
        }

        /// <summary>
        /// 添加节点
        /// </summary>
        [HttpPost("add")]
        public async Task<ResultDto<SysMenuDto>> Add([FromBody] SysMenuDto input)
        {
            return await _sysMenuAppService.Add(input);
        }

        /// <summary>
        /// 更新节点
        /// </summary>
        [HttpPut("update")]
        public async Task<ResultDto<SysMenuDto>> Update([FromBody] SysMenuDto input)
        {
            return await _sysMenuAppService.Update(input);
        }

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="ids">id集合</param>
        [HttpDelete("remove")]
        public async Task<ResultDto<string>> Remove([FromBody] List<int> ids)
        {
            return await _sysMenuAppService.Remove(ids);
        }

        /// <summary>
        /// 获取树节点
        /// </summary>
        [HttpGet("get-tree-nodes")]
        public async Task<ResultDto<List<SysMenuOutputDto>>> GetTreeNodes(SysMenuInputDto input)
        {
            return await _sysMenuAppService.GetTreeNodes(input);
        }

        /// <summary>
        /// 获取菜单导航
        /// </summary>
        [HttpGet("get-menu-path")]
        public async Task<ResultDto<List<SysMenuOutputDto>>> GetMenuPath(int id)
        {
            return await _sysMenuAppService.GetMenuPath(id);
        }
    }
}
