﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Niaofei.Application.Contracts;

namespace Niaofei.HttpApi.Controllers
{
    /// <summary>
    /// 部门管理
    /// </summary>
    [Authorize]
    [Route("api/sys-dept")]
    public class SysDeptController : ControllerBase
    {
        private readonly ISysDeptAppService _sysDeptAppService;

        public SysDeptController(ISysDeptAppService sysDeptAppService)
        {
            _sysDeptAppService = sysDeptAppService;
        }

        /// <summary>
        /// 部门添加
        /// </summary>
        [HttpPost("add-sys-dept")]
        public async Task<ResultDto<SysDeptDto>> AddSysDept([FromBody] SysDeptCreateDto input)
        {
            return await _sysDeptAppService.AddSysDept(input);
        }

        /// <summary>
        /// 部门获取
        /// </summary>
        [HttpGet("get-sys-dept")]
        public async Task<ResultDto<List<SysDeptTreeDto>>> GetSysDept(SysDeptInputDto input)
        {
            return await _sysDeptAppService.GetSysDept(input);
        }

        /// <summary>
        /// 部门更新
        /// </summary>
        [HttpPut("update-sys-dept")]
        public async Task<ResultDto<SysDeptDto>> UpdateSysDept([FromBody] SysDeptUpdateDto input)
        {
            return await _sysDeptAppService.UpdateSysDept(input);
        }

        /// <summary>
        /// 删除部门
        /// </summary>
        [HttpDelete("delete-sys-dept")]
        public async Task<ResultDto> DeleteSysDept([FromBody] List<long> ids)
        {
            return await _sysDeptAppService.DeleteSysDept(ids);
        }
    }
}
