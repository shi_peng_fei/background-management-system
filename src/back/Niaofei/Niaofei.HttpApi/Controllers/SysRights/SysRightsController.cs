﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Niaofei.Application.Contracts;

namespace Niaofei.HttpApi.Controllers
{
    /// <summary>
    /// 权限管理
    /// </summary>
    [Authorize]
    [Route("api/sys-rights")]
    public class SysRightsController : ControllerBase
    {
        private readonly ISysRightsAppService _sysRightsAppService;

        public SysRightsController(ISysRightsAppService sysRightsAppService)
        {
            _sysRightsAppService = sysRightsAppService;
        }

        /// <summary>
        /// 更新权限
        /// </summary>
        [HttpPut("update-sys-rights")]
        public async Task UpdateSysRights([FromBody] List<SysRightsUpdateDto> input)
        {
            await _sysRightsAppService.UpdateSysRights(input);
        }

        /// <summary>
        /// 获取系统权限
        /// </summary>
        [HttpGet("get-sys-rights")]
        public async Task<ResultDto<List<SysRightsDto>>> GetSysRights(SysRightsInputDto input)
        {
            return await _sysRightsAppService.GetSysRights(input);
        }

        /// <summary>
        /// 获取当前用户权限
        /// </summary>
        [HttpGet("get-current-user-rights/{menuId}")]
        public async Task<ResultDto<List<SysRightsDto>>> GetCurrentUserRights(int? menuId)
        {
            return await _sysRightsAppService.GetCurrentUserRights(menuId);
        }

        /// <summary>
        /// 添加系统权限
        /// </summary>
        [HttpPost("add-sys-rights")]
        public async Task AddSysRights([FromBody] SysRightsAddDto input)
        {
            await _sysRightsAppService.AddSysRights(input);
        }
    }
}
