﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace Niaofei.HttpApi
{
    /// <summary>
    /// 天气相关控制器
    /// </summary>
    [ApiController]
    [Route("api/weather-forecast")]
    [ApiExplorerSettings(GroupName = "v2")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly IMapper _mapper;
        private readonly IDistributedCache _cache;
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IMapper mapper, IDistributedCache cache)
        {
            _cache = cache;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// 获取天气
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet("test-redis")]
        public async Task TestRedis()
        {
            await _cache.SetStringAsync("spf", "石鹏飞存的redis");

            var result = await _cache.GetStringAsync("spf");

            _logger.LogInformation("redis：" + result);
        }
    }
}
