﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Niaofei.Application.Contracts;

namespace Niaofei.HttpApi
{
    /// <summary>
    /// 系统用户
    /// </summary>
    [Authorize]
    [Route("api/sys-user")]
    public class SysUserController : ControllerBase
    {
        private readonly ISysUserAppService _sysUserAppService;

        public SysUserController(IServiceProvider provider)
        {
            _sysUserAppService = provider.GetRequiredService<ISysUserAppService>();
        }

        /// <summary>
        /// 获取用户
        /// </summary>
        [HttpGet("get-user")]
        public async Task<ResultDto<SysUserOutputDto>> GetUser(long id)
        {
            return await _sysUserAppService.GetUser(id);
        }

        /// <summary>
        /// 获取当前用户
        /// </summary>
        [HttpGet("get-current-user")]
        public async Task<ResultDto<SysUserOutputDto>> GetUser()
        {
            return await _sysUserAppService.GetUser();
        }

        /// <summary>
        /// 用户查询页面
        /// </summary>
        [HttpGet("get-user-page")]
        public async Task<ResultDto<PageOutputDto<SysUserOutputDto>>> GetUserPage(SysUserInputDto input)
        {
            return await _sysUserAppService.GetUserPage(input);
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        [HttpPost("add-user")]
        public async Task<ResultDto<SysUserOutputDto>> AddUser([FromBody] SysUserAddDto input)
        {
            return await _sysUserAppService.AddUser(input);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        [HttpDelete("remove-user")]
        public async Task<ResultDto<bool>> RemoveUser(long id)
        {
            return await _sysUserAppService.RemoveUser(id);
        }

        /// <summary>
        /// 更新用户
        /// </summary>
        [HttpPut("update-user")]
        public async Task<ResultDto<SysUserOutputDto>> UpdateUser([FromBody] SysUserUpdateDto input)
        {
            return await _sysUserAppService.UpdateUser(input);
        }

        /// <summary>
        /// 登录
        /// </summary>
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ResultDto<string>> Login(SysUserLoginInputDto input)
        {
            return await _sysUserAppService.Login(input);
        }

        /// <summary>
        /// 获取用户头像
        /// </summary>
        [AllowAnonymous]
        [HttpGet("get-user-avatar")]
        public async Task<ResultDto<string>> GetUserAvatar(string userName)
        {
            return await _sysUserAppService.GetUserAvatar(userName);
        }
    }
}
