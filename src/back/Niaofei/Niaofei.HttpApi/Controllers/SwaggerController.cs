﻿using Microsoft.AspNetCore.Mvc;
using Niaofei.Application.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.HttpApi.Controllers
{
    /// <summary>
    /// Swagger重定向
    /// </summary>
    [ApiExplorerSettings(GroupName = "v2")]
    public class SwaggerController : ControllerBase
    {
        private readonly ITestAppService _testAppService;

        public SwaggerController(ITestAppService testAppService)
        {
            _testAppService = testAppService;
        }

        [HttpGet("/")]
        public IActionResult RedirectSwagger()
        {
            return Redirect("/swagger/index.html");
        }

        /// <summary>
        /// 自增测试
        /// </summary>
        [HttpGet("api/add/test")]
        public Task Test()
        {
            return _testAppService.Test();
        }
    }
}
