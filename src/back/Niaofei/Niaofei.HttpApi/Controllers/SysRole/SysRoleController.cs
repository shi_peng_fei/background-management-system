﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Niaofei.Application.Contracts;

namespace Niaofei.HttpApi
{
    /// <summary>
    /// 用户角色
    /// </summary>
    [Authorize]
    [Route("api/sys-role")]
    public class SysRoleController : ControllerBase
    {
        private readonly ISysRoleAppService _sysRoleAppService;

        public SysRoleController(ISysRoleAppService sysRoleAppService)
        {
            _sysRoleAppService = sysRoleAppService;
        }

        /// <summary>
        /// 添加用户角色
        /// </summary>
        [HttpPost("add-sys-role")]
        public async Task<ResultDto<SysRoleDto>> AddSysRole([FromBody] SysRoleDto input)
        {
            return await _sysRoleAppService.AddSysRole(input);
        }

        /// <summary>
        /// 修改用户角色
        /// </summary>
        [HttpPut("update-sys-user")]
        public async Task<ResultDto<SysRoleDto>> UpdateRole([FromBody] SysRoleDto input)
        {
            return await _sysRoleAppService.UpdateRole(input);
        }

        /// <summary>
        /// 用户角色查询
        /// </summary>
        [HttpGet("get-sys-role-page")]
        public async Task<ResultDto<PageOutputDto<SysRoleDto>>> GetSysRolePage(SysRoleInputDto input)
        {
            return await _sysRoleAppService.GetSysRolePage(input);
        }

        /// <summary>
        /// 获取用户角色
        /// </summary>
        [HttpGet("get-sys-role")]
        public async Task<ResultDto<List<SysRoleDto>>> GetSysRole()
        {
            return await _sysRoleAppService.GetSysRole();
        }

        /// <summary>
        /// 删除用户角色
        /// </summary>
        [HttpDelete("delete-sys-role")]
        public async Task<ResultDto<string>> DeleteSysRole([FromBody] List<int> ids)
        {
            return await _sysRoleAppService.DeleteSysRole(ids);
        }
    }
}
