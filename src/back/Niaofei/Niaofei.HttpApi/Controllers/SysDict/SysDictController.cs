﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Niaofei.Application;
using Niaofei.Application.Contracts;

namespace Niaofei.HttpApi
{
    /// <summary>
    /// 数据字典
    /// </summary>
    [Authorize]
    [Route("api/sys-dict")]
    public class SysDictController : ControllerBase
    {
        private readonly ISysDictAppService _sysDictAppService;

        public SysDictController(IServiceProvider serviceProvider)
        {
            _sysDictAppService = serviceProvider.GetRequiredService<ISysDictAppService>();
        }

        /// <summary>
        /// 类型添加
        /// </summary>
        [HttpPost("add-dict-type")]
        public async Task<ResultDto<SysDictTypeDto>> AddDictType([FromBody] SysDictTypeDto input)
        {
            return await _sysDictAppService.AddDictType(input);
        }

        /// <summary>
        /// 类型获取
        /// </summary>
        [HttpGet("get-dict-type")]
        public async Task<ResultDto<SysDictTypeDto>> GetDictType(SysDictTypeInputDto input)
        {
            return await _sysDictAppService.GetDictType(input);
        }

        /// <summary>
        /// 类型移除
        /// </summary>
        [HttpDelete("remove-dict-type")]
        public async Task<ResultDto> RemoveDictType([FromBody] List<int> ids)
        {
            return await _sysDictAppService.RemoveDictType(ids);
        }

        /// <summary>
        /// 类型更新
        /// </summary>
        [HttpPut("update-dict-type")]
        public async Task<ResultDto<SysDictTypeDto>> UpdateDictType([FromBody] SysDictTypeDto input)
        {
            return await _sysDictAppService.UpdateDictType(input);
        }

        /// <summary>
        /// 获取类型列表
        /// </summary>
        [HttpGet("get-dict-type-page")]
        public async Task<ResultDto<PageOutputDto<SysDictTypeDto>>> GetDictTypePage(SysDictTypePageInputDto input)
        {
            return await _sysDictAppService.GetDictTypePage(input);
        }

        /// <summary>
        /// 获取数据字典列表
        /// </summary>
        [HttpGet("get-dict-page")]
        public async Task<ResultDto<PageOutputDto<SysDictDto>>> GetDictPage(SysDictInputDto input)
        {
            return await _sysDictAppService.GetDictPage(input);
        }

        /// <summary>
        /// 添加数据字典
        /// </summary>
        [HttpPost("add-dict")]
        public async Task<ResultDto<SysDictDto>> AddDict([FromBody] SysDictDto input)
        {
            return await _sysDictAppService.AddDict(input);
        }

        /// <summary>
        /// 修改数据字典
        /// </summary>
        [HttpPut("update-dict")]
        public async Task<ResultDto<SysDictDto>> UpdateDict([FromBody] SysDictDto input)
        {
            return await _sysDictAppService.UpdateDict(input);
        }

        /// <summary>
        /// 删除数据字典
        /// </summary>
        [HttpDelete("remove-dict")]
        public async Task<ResultDto> RemoveDict([FromBody] List<int> ids)
        {
            return await _sysDictAppService.RemoveDict(ids);
        }
    }
}
