﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Minio.DataModel;
using Niaofei.Application.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.HttpApi
{
    /// <summary>
    /// 文件管理
    /// </summary>
    [Route("api/file-manage")]
    public class FileManageController : ControllerBase
    {
        private readonly IFileManageAppService _fileManageAppService;

        public FileManageController(IFileManageAppService fileManageAppService)
        {
            _fileManageAppService = fileManageAppService;
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        [HttpPost("upload-file")]
        public async Task<ResultDto<string>> UploadFile(IFormFile file)
        {
            return await _fileManageAppService.UploadFile(file);
        }

        /// <summary>
        /// 获取文件
        /// </summary>
        [HttpGet("get-file/{bucketName}/{fileName}")]
        [ProducesResponseType(typeof(FileStreamResult), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFile(string bucketName, string fileName)
        {
            var result = await _fileManageAppService.GetFile(bucketName, fileName);

            return new FileStreamResult(new MemoryStream(result.Data), result.ContentType);
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        [HttpGet("download/{bucketName}/{fileName}")]
        [ProducesResponseType(typeof(FileResult), StatusCodes.Status200OK)]
        public async Task<IActionResult> Download(string bucketName, string fileName)
        {
            var result = await _fileManageAppService.GetFile(bucketName, fileName);

            return File(result.Data, result.ContentType, result.FileName);
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        [HttpDelete("delete-file/{bucketName}/{fileName}")]
        public async Task DeleteFile(string bucketName, string fileName)
        {
            await _fileManageAppService.DeleteFile(bucketName, fileName);
        }

        /// <summary>
        /// 获取文件信息
        /// </summary>
        [HttpGet("delete-file/{bucketName}/{fileName}")]
        public async Task<ObjectStat?> GetFileInfo(string bucketName, string fileName)
        {
            return await _fileManageAppService.GetFileInfo(bucketName, fileName);
        }

        /// <summary>
        /// 获取验证码图片
        /// </summary>
        [HttpGet("get-verification-code")]
        public async Task<IActionResult> GetVerificationCode()
        {
            var result = await _fileManageAppService.GetVerificationCode();

            return File(result, "image/jpeg");
        }
    }
}
