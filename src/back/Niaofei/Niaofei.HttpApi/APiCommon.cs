﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.HttpApi
{
    public static class APiCommon
    {
        public static string[] GetXmlDocument()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;

            var files = Directory.GetFiles(path, "Niaofei.*.xml", SearchOption.AllDirectories);

            return files;
        }
    }
}
