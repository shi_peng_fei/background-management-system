﻿using Microsoft.AspNetCore.Authorization;

namespace Niaofei.Web
{
    public class CustomHandler : AuthorizationHandler<CustomRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomRequirement requirement)
        {
            // 在此处进行自定义的授权逻辑
            // 使用 context 对象来检查用户信息、请求参数等，以确定是否满足授权条件
            // 如果满足条件，调用 context.Succeed(requirement) 表示授权成功；否则，调用 context.Fail() 表示授权失败

            context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
