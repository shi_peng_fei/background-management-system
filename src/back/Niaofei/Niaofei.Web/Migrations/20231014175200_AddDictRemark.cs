﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Niaofei.Web.Migrations
{
    /// <inheritdoc />
    public partial class AddDictRemark : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Remark",
                table: "sys_dict_type",
                type: "varchar(2000)",
                maxLength: 2000,
                nullable: false,
                defaultValue: "",
                comment: "备注")
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remark",
                table: "sys_dict_type");
        }
    }
}
