﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Niaofei.Web.Migrations
{
    /// <inheritdoc />
    public partial class _2024042901 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_sys-rights",
                table: "sys-rights");

            migrationBuilder.RenameTable(
                name: "sys-rights",
                newName: "sys_rights");

            migrationBuilder.AddPrimaryKey(
                name: "PK_sys_rights",
                table: "sys_rights",
                column: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_sys_rights",
                table: "sys_rights");

            migrationBuilder.RenameTable(
                name: "sys_rights",
                newName: "sys-rights");

            migrationBuilder.AddPrimaryKey(
                name: "PK_sys-rights",
                table: "sys-rights",
                column: "Id");
        }
    }
}
