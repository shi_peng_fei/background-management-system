﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Niaofei.Web.Migrations
{
    /// <inheritdoc />
    public partial class _2024111401 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Perm",
                table: "sys_menu");

            migrationBuilder.AlterColumn<int>(
                name: "ParentId",
                table: "sys_dept",
                type: "int",
                nullable: true,
                comment: "上级部门",
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true,
                oldComment: "上级部门");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "sys_dept",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Perm",
                table: "sys_menu",
                type: "varchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "",
                comment: "权限标识")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                table: "sys_dept",
                type: "bigint",
                nullable: true,
                comment: "上级部门",
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true,
                oldComment: "上级部门");

            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "sys_dept",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn);
        }
    }
}
