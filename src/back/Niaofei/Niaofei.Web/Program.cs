using Niaofei.Application;
using Niaofei.Domain.Shared;
using Niaofei.HttpApi;
using Niaofei.Redis;
using Serilog;
using Serilog.Events;
using System.Diagnostics;

namespace Niaofei.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateBootstrapLogger();

            //Debugger.Launch();

            var builder = WebApplication.CreateBuilder(args);

            builder.Host.UseSerilog((context, services, configuration) =>
                configuration.ReadFrom.Configuration(context.Configuration)
                .ReadFrom.Services(services));

            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddHttpContextAccessor();

            //自定义服务配置
            builder.Services.AddCors();
            builder.Services.AddSwaggerGen();
            builder.Services.AddRedisCache();
            builder.Services.AddAutoMapperServices();
            builder.Services.AddControllersAndSetJsonOption();
            builder.Services.AddMinio(builder.Configuration);
            builder.Services.AddDbContext(builder.Configuration);
            builder.Services.SetAppSetting(builder.Configuration);
            builder.Services.AddAuthentication(builder.Configuration);
            builder.Services.LoadAssemblys();

            var app = builder.Build();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                    c.SwaggerEndpoint("/swagger/v2/swagger.json", "My API V2");
                });
            }

            app.LogMapper();
            app.UseMiddleware<AutoSaveMiddleware>();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseAuthorization();
            app.MapControllers();
            app.UseCors();

            app.Run();
        }
    }
}