﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class DefaultDto<T> : EntityDto<T>
    {
        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }

        public string UserId { get; set; }
    }
}
