﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class EntityDto<T>
    {
        /// <summary>
        /// 主键
        /// </summary>
        public T Id { get; set; }
    }
}
