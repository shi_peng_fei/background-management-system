﻿namespace Niaofei.Application.Contracts
{
    public class ResultDto<TData>
    {
        public ResultDto()
        {

        }

        public ResultDto(TData data)
        {
            Code = "0";
            Data = data;
        }

        public ResultDto(string message)
        {
            Code = "1";
            Message = message;
        }

        /// <summary>
        /// 成功-0,其他都为失败
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public TData Data { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }
    }

    public class ResultDto : ResultDto<object>
    {

    }
}
