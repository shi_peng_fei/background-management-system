﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class PageOutputDto<TList>
    {
        /// <summary>
        /// 总数
        /// </summary>
        public long Count { get; set; }

        /// <summary>
        /// 列表
        /// </summary>
        public IList<TList> List { get; set; }

        public PageOutputDto(long count,IList<TList> list)
        {
            Count = count;
            List = list;
        }

        public PageOutputDto()
        {
            
        }
    }
}
