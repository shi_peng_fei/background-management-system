﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysDictInputDto : PageInputDto
    {
        /// <summary>
        /// 数据字典类型Id
        /// </summary>
        public int SysDictTypeId { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string KeyWord { get; set; }

        /// <summary>
        /// 状态：启用-1，禁用-0
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 是否默认
        /// </summary>
        public bool? IsDefault { get; set; }
    }
}
