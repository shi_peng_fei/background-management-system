﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysDictTypePageInputDto : PageInputDto
    {
        /// <summary>
        /// Id查询
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Code查询
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Name查询
        /// </summary>
        public string Name { get; set; }
    }
}
