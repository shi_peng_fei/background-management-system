﻿namespace Niaofei.Application.Contracts
{
    public class SysDictDto : DefaultDto<int?>
    {
        /// <summary>
        /// 数据字典类型Id
        /// </summary>
        public int SysDictTypeId { get; set; }

        /// <summary>
        /// 字典类型编码
        /// </summary>
        public string TypeCode { get; set; }

        /// <summary>
        /// 字典项名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 字段项值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态：启用-1，禁用-0
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
