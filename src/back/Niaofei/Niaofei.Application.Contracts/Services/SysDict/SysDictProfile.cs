﻿using AutoMapper;
using Niaofei.Application.Contracts;
using Niaofei.Domain;

namespace Niaofei.Application
{
    public class SysDictProfile : Profile
    {
        public SysDictProfile()
        {
            CreateMap<SysDictTypeDto, SysDictType>(MemberList.Source)
                .ForMember(dest => dest.Id, opt => opt.Ignore()).ReverseMap();
            CreateMap<SysDict, SysDictDto>(MemberList.None).ReverseMap();
        }
    }
}
