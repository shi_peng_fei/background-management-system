﻿using Niaofei.Application.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application
{
    public interface ISysDictAppService
    {
        Task<ResultDto<PageOutputDto<SysDictTypeDto>>> GetDictTypePage(SysDictTypePageInputDto input);

        Task<ResultDto<SysDictTypeDto>> GetDictType(SysDictTypeInputDto input);

        Task<ResultDto<SysDictTypeDto>> AddDictType(SysDictTypeDto input);

        Task<ResultDto<SysDictTypeDto>> UpdateDictType(SysDictTypeDto input);

        Task<ResultDto> RemoveDictType(List<int> ids);

        Task<ResultDto<PageOutputDto<SysDictDto>>> GetDictPage(SysDictInputDto input);

        Task<ResultDto<SysDictDto>> AddDict(SysDictDto input);

        Task<ResultDto<SysDictDto>> UpdateDict(SysDictDto input);

        Task<ResultDto> RemoveDict(List<int> ids);
    }
}
