﻿using AutoMapper;
using Niaofei.Domain;

namespace Niaofei.Application.Contracts
{
    public class SysRoleProfile : Profile
    {
        public SysRoleProfile()
        {
            CreateMap<SysRole, SysRoleDto>(MemberList.None).ReverseMap();
        }
    }
}
