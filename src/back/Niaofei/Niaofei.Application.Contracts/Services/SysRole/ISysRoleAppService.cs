﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public interface ISysRoleAppService
    {
        Task<ResultDto<SysRoleDto>> AddSysRole(SysRoleDto input);
        Task<ResultDto<SysRoleDto>> UpdateRole(SysRoleDto input);
        Task<ResultDto<PageOutputDto<SysRoleDto>>> GetSysRolePage(SysRoleInputDto input);
        Task<ResultDto<string>> DeleteSysRole(List<int> ids);
        Task<ResultDto<List<SysRoleDto>>> GetSysRole();
    }
}
