﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysRoleDto : EntityDto<int>
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 角色编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 角色状态：正常-1, 停用-0
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 权限数据
        /// </summary>
        public int DataScope { get; set; }
    }
}
