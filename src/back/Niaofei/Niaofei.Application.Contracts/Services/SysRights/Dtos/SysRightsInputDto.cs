﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysRightsInputDto
    {
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int? MenuId { get; set; }

        /// <summary>
        /// 角色Id
        /// </summary>
        public int? RoleId { get; set; }
    }
}
