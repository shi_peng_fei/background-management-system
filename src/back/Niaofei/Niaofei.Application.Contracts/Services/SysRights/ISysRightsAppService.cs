﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public interface ISysRightsAppService
    {
        Task AddSysRights(SysRightsAddDto input);
        Task UpdateSysRights(List<SysRightsUpdateDto> input);
        Task<ResultDto<List<SysRightsDto>>> GetSysRights(SysRightsInputDto input);
        Task<ResultDto<List<SysRightsDto>>> GetCurrentUserRights(int? menuId);
    }
}
