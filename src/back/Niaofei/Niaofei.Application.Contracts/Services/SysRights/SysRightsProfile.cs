﻿using AutoMapper;
using Niaofei.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysRightsProfile : Profile
    {
        public SysRightsProfile()
        {
            CreateMap<SysRightsDto, SysRights>(MemberList.None).ReverseMap();
            CreateMap<SysRightsUpdateDto, SysRights>(MemberList.None);
            CreateMap<SysRightsAddDto, SysRights>(MemberList.None);
        }
    }
}
