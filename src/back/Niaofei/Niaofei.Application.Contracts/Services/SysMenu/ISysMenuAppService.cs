﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public interface ISysMenuAppService
    {
        Task<ResultDto<SysMenuDto>> Add(SysMenuDto input);
        Task<ResultDto<SysMenuDto>> Update(SysMenuDto input);
        Task<ResultDto<string>> Remove(List<int> ids);
        Task<ResultDto<List<SysMenuOutputDto>>> GetTreeNodes(SysMenuInputDto input);
        Task<ResultDto<List<SysMenuOutputDto>>> GetMenuPath(int id);
    }
}
