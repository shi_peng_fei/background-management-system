﻿using AutoMapper;
using Niaofei.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    internal class SysMenuProfile : Profile
    {
        public SysMenuProfile()
        {
            CreateMap<SysMenu,SysMenuDto>().ReverseMap();
            CreateMap<SysMenu,SysMenuOutputDto>();
        }
    }
}
