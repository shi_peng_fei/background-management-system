﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysMenuInputDto
    {
        /// <summary>
        /// 是否显示隐藏nemu
        /// </summary>
        public bool IsVisible { get; set; }
    }
}
