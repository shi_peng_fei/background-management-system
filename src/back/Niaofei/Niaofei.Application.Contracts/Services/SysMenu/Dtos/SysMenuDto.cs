﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysMenuDto : EntityDto<int>
    {
        /// <summary>
        /// 父级菜单
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 菜单类型
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 浏览器地址
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 组件名称
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// 显示标识
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 外链接路径
        /// </summary>
        public string RedirectUrl { get; set; }

        /// <summary>
        /// 跳转路径
        /// </summary>
        public string Redirect { get; set; }
    }
}
