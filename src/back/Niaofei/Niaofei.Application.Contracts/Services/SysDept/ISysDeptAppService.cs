﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public interface ISysDeptAppService
    {
        Task<ResultDto<SysDeptDto>> AddSysDept(SysDeptCreateDto input);
        Task<ResultDto<List<SysDeptTreeDto>>> GetSysDept(SysDeptInputDto input);
        Task<ResultDto<SysDeptDto>> UpdateSysDept(SysDeptUpdateDto input);
        Task<ResultDto> DeleteSysDept(List<long> ids);
    }
}
