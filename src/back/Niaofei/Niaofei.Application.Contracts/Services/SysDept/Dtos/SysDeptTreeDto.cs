﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysDeptTreeDto : DefaultDto<int>
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 上级部门
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// 部门路径
        /// </summary>
        public string? TreePath { get; set; }

        /// <summary>
        /// 显示顺序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态：0-正常, 1-禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        public List<SysDeptTreeDto> Children { get; set; } = new();

        public void SetChiren(List<SysDeptTreeDto> depts)
        {
            Children ??= new();

            foreach (var dept in depts)
            {
                if (dept.ParentId == this.Id)
                {
                    dept.TreePath = this.TreePath + $",{dept.Name}";
                    this.Children.Add(dept);
                }
            }

            this.Children.ForEach(m => m.SetChiren(depts));
            this.Children = this.Children.OrderBy(m => m.Sort).ToList();
        }
    }
}
