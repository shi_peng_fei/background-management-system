﻿using AutoMapper;
using Niaofei.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysDeptProfile : Profile
    {
        public SysDeptProfile()
        {
            CreateMap<SysDeptDto, SysDept>(MemberList.None).ReverseMap();
            CreateMap<SysDept, SysDeptTreeDto>(MemberList.None);
            CreateMap<SysDeptCreateDto, SysDept>(MemberList.None);
            CreateMap<SysDeptUpdateDto, SysDept>(MemberList.None);
        }
    }
}
