﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class SysUserInputDto : PageInputDto
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string KeyWord { get; set; }

        /// <summary>
        /// 部门Id
        /// </summary>
        public int? DeptId { get; set; }
    }
}
