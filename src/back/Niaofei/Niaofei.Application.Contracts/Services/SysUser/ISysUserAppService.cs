﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public interface ISysUserAppService
    {
        Task<ResultDto<SysUserOutputDto>> GetUser(long id);
        Task<ResultDto<SysUserOutputDto>> GetUser();
        Task<ResultDto<SysUserOutputDto>> AddUser(SysUserAddDto input);
        Task<ResultDto<bool>> RemoveUser(long id);
        Task<ResultDto<SysUserOutputDto>> UpdateUser(SysUserUpdateDto input);
        Task<ResultDto<PageOutputDto<SysUserOutputDto>>> GetUserPage(SysUserInputDto input);
        Task<ResultDto<string>> Login(SysUserLoginInputDto input);
        Task<ResultDto<string>> GetUserAvatar(string userName);
    }
}
