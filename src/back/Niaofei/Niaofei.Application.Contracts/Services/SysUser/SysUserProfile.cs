﻿using AutoMapper;
using Niaofei.Application.Contracts;
using Niaofei.Domain;

namespace Niaofei.Application
{
    public class SysUserProfile : Profile
    {
        public SysUserProfile()
        {
            CreateMap<SysUser, SysUserDto>();
            CreateMap<SysUser, SysUserOutputDto>();
            CreateMap<SysUserAddDto, SysUser>(MemberList.None);
            CreateMap<SysUserUpdateDto, SysUser>(MemberList.None);
        }
    }
}
