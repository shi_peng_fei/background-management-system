﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public class FileManageOutputDto
    {
        /// <summary>
        /// 文件数据
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 返回类型
        /// </summary>
        public string ContentType { get; set; }
    }
}
