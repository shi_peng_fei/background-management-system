﻿using Microsoft.AspNetCore.Http;
using Minio.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Application.Contracts
{
    public interface IFileManageAppService
    {
        Task<ResultDto<string>> UploadFile(IFormFile file);

        Task<FileManageOutputDto> GetFile(string bucketName, string fileName);

        Task DeleteFile(string bucketName, string fileName);

        Task<ObjectStat?> GetFileInfo(string bucketName, string fileName);

        Task<byte[]> GetVerificationCode();
    }
}
