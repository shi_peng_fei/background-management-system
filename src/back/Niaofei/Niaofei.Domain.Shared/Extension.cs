﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Niaofei.Domain.Shared
{
    public static class Extension
    {
        /// <summary>
        /// 系统配置管理
        /// </summary>
        public static void SetAppSetting(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<AuthorizationOptions>(configuration.GetSection(AuthorizationOptions.Options));
            services.Configure<MinioOption>(configuration.GetSection(MinioOption.Options));
        }
    }
}
