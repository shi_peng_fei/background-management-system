﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Domain.Shared
{
    public class MinioOption
    {
        /// <summary>
        /// option节点
        /// </summary>
        public static string Options = "Minio";

        /// <summary>
        /// Minio地址
        /// </summary>
        public string Endpoint { get; set; }

        /// <summary>
        /// AccessKey
        /// </summary>
        public string AccessKey { get; set; }

        /// <summary>
        /// SecretKey
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// IsSSL
        /// </summary>
        public bool IsSSL { get; set; }

        /// <summary>
        /// 默认桶
        /// </summary>
        public string DefaultBucket { get; set; }
    }
}
