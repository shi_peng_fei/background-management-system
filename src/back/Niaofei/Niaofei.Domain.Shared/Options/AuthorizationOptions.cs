﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Domain.Shared
{
    public class AuthorizationOptions
    {
        /// <summary>
        /// option节点
        /// </summary>
        public static string Options = "Authorization";

        /// <summary>
        /// 身份认证密钥
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// 实体令牌：服务器地址
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// 客户端地址
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// 有效期：分钟
        /// </summary>
        public int ExpirationMinutes { get; set; }
    }
}
