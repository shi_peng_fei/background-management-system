## 组织架构图

```mermaid
flowchart LR
Web --> HttpApi --> Appliaction.Contracts --> Domain.Shared --> CommonProject
Web --> Appliaction --> Appliaction.Contracts
Web --> EFCore --> Domain --> Domain.Shared
Appliaction --> Domain



```

## 实体关系图

### 数据字典

```mermaid
erDiagram
sysDict{
	int Id pk "主键"
	int SysDictTypeId "数据字典类型Id"
	string TypeCode "字典类型编码"
	string Name "字典项名称"
	string Value "字典项值"
	int Sort "排序"
	int Status "状态：启用-1，禁用-0"
	bool IsDefault "是否默认"
	string Remark "备注"
}
sysDictType {
	int Id pk "主键"
	string Code "类型编码"
	string Name "类型名称"
	int Status "状态：0-正常，1-禁用"
	string Remark "备注"
}

sysDict }o -- |o sysDictType:SysDictTypeId-Id
```

---

### 菜单

```mermaid
erDiagram
SysMenu{
	int Id pk "主键"
	int ParentId fk "父级菜单"
	string Name "菜单名称"
	int Type "菜单类型"
	string Path "浏览器地址"
	string Component "组件名称"
	string Prem "权限标识"
	bool Visible "显示标识"
	int Sort "排序"
	string Icon "图标"
	string RedirectUrl "外链路径"
	string Redirect "跳转路径"
}

SysMenu }o -- || SysMenu:ParentId-Id
```

---

### 用户管理

```mermaid
erDiagram
SysRole {
	int Id pk "主键"
	string Name "角色名称"
	string Code "角色编码"
	int Sort "排序"
	int Status "角色状态：正常-1，停用-0"
	int DataScope "权限数据"
}

SysDept {
	int Id pk "主键"
	string ParentId fk "上级部门"
	string Name "部门名称"
	string TreePath "部门路径"
	int Sort "排序"
	int Status "状态：1-正常，0-禁用"
}

SysUser {
	int Id pk "主键"
    int DeptId fk "部门Id"
	string UserName "用户姓名"
	string NickName "用户名称"
	int Gener "性别"
	int Age "年龄"
	string PassWord "密码"
	string Avatar "头像"
	string Mobile "联系方式"
	string Email "邮箱"
	int Status "状态：1-正常，0禁用"
}

SysUserRole {
	int Id pk "主键"
	int UserId fk "用户Id"
	int RoleId fk "角色Id"
}

SysDept o| -- o{ SysUser :"Id-DeptId"
SysUserRole o| -- o{ SysRole:"RoleId-Id"
SysUserRole o| -- o{ SysUser:"UserId-Id"
```

---

### 工作流