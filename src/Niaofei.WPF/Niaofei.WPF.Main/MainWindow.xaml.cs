﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Niaofei.Sdk;
using System.Windows;

namespace Niaofei.WPF.Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [Injection]
    public partial class MainWindow : Window
    {
        private readonly IServiceProvider _serviceProvider;
        public List<TreeItem> TreeData { get; set; }

        public MainWindow(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            InitializeComponent();
            _serviceProvider = serviceProvider;

            Loaded += this.OnLoad;
            Title = configuration["Name"];

            TreeData = new List<TreeItem>
            {
                new TreeItem
                {
                    Name = "根节点 1",
                    Children = new List<TreeItem>
                    {
                        new TreeItem { Name = "子节点 1.1" },
                        new TreeItem { Name = "子节点 1.2" }
                    }
                },
                new TreeItem
                {
                    Name = "根节点 2",
                    Children = new List<TreeItem>
                    {
                        new TreeItem { Name = "子节点 2.1" },
                        new TreeItem
                        {
                            Name = "子节点 2.2",
                            Children = new List<TreeItem>
                            {
                                new TreeItem { Name = "子节点 2.2.1" },
                                new TreeItem { Name = "子节点 2.2.2" }
                            }
                        }
                    }
                }
            };

            DataContext = this;
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            var menus = _serviceProvider.GetServices<MenuCommand>();
        }
    }

    public class TreeItem
    {
        public string? Name { get; set; }
        public List<TreeItem> Children { get; set; }

        public TreeItem()
        {
            Children = new List<TreeItem>();
        }
    }
}