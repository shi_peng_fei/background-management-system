﻿using System.Globalization;
using System.Windows.Data;

namespace Niaofei.WPF.Control
{
    public class DoubleToHalfConverter : IValueConverter
    {
        public static DoubleToHalfConverter Instance { get => new DoubleToHalfConverter(); }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double doubleValue)
            {
                return doubleValue / 2;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
