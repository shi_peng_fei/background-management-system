﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Niaofei.WPF.Control
{
    public static class BorderHelper
    {
        public static readonly DependencyProperty ClipRadiusProperty =
            DependencyProperty.RegisterAttached(
                "ClipRadius",
                typeof(bool),
                typeof(BorderHelper),
                new FrameworkPropertyMetadata(default(bool),
                    FrameworkPropertyMetadataOptions.AffectsArrange|
                    FrameworkPropertyMetadataOptions.AffectsMeasure,
                    OnClipOpaclityMaskChanged));

        private static void OnClipOpaclityMaskChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is not Border border) 
            {
                return;
            }

            if (!border.IsLoaded)
            {
                border.Loaded += BorderOnLoaded;
            }
        }

        private static void UpdateOpacityMask(Border border)
        {
            if (!border.IsLoaded)
            {
                return;
            }

            var visualBrush = new VisualBrush();
            var innerBorder = new Border()
            {
                 Width = border.ActualWidth,
                 Height = border.ActualHeight,
                 BorderThickness = border.BorderThickness,
                 CornerRadius = border.CornerRadius,
                 Background = Brushes.White,
            };

            visualBrush.Visual = innerBorder;
            border.OpacityMask = visualBrush;

            border.Loaded -= BorderOnLoaded;
        }

        private static void BorderOnLoaded(object sender, RoutedEventArgs e)
        {
            if (sender is Border border)
            {
                UpdateOpacityMask(border);
            }
        }

        public static bool GetClipRadius(DependencyObject obj)
        {
            return (bool)obj.GetValue(ClipRadiusProperty);
        }

        public static void SetClipRadius(DependencyObject obj, bool value)
        {
            obj.SetValue(ClipRadiusProperty, value);
        }
    }
}
