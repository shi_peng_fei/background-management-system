﻿using System.Windows;

namespace Niaofei.WPF.Control
{
    public static class CheckBoxHelper
    {
        public static CornerRadius GetIconCornerRadius(DependencyObject obj)
        {
            return (CornerRadius)obj.GetValue(IconCornerRadiusProperty);
        }
        public static void SetIconCornerRadius(DependencyObject obj, CornerRadius value)
        {
            obj.SetValue(IconCornerRadiusProperty, value);
        }
        /// <summary>
        /// Icon圆角
        /// </summary>
        public static readonly DependencyProperty IconCornerRadiusProperty =
            DependencyProperty.RegisterAttached(
                "IconCornerRadius",
                typeof(CornerRadius),
                typeof(CheckBoxHelper),
                new PropertyMetadata(null));

        public static Thickness GetIconThickness(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(IconThicknessProperty);
        }
        public static void SetIconThickness(DependencyObject obj, Thickness value)
        {
            obj.SetValue(IconThicknessProperty, value);
        }
        /// <summary>
        /// Icon边框
        /// </summary>
        public static readonly DependencyProperty IconThicknessProperty =
            DependencyProperty.RegisterAttached(
                "IconThickness",
                typeof(Thickness),
                typeof(CheckBoxHelper),
                new PropertyMetadata(null));

        public static Thickness GetContentMargin(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(ContentMarginProperty);
        }
        public static void SetContentMargin(DependencyObject obj, Thickness value)
        {
            obj.SetValue(ContentMarginProperty, value);
        }
        /// <summary>
        /// 内容Margin
        /// </summary>
        public static readonly DependencyProperty ContentMarginProperty =
            DependencyProperty.RegisterAttached(
                "ContentMargin",
                typeof(Thickness),
                typeof(CheckBoxHelper),
                new PropertyMetadata(null));

        public static double GetIconWidth(DependencyObject obj)
        {
            return (double)obj.GetValue(IconWidthProperty);
        }
        public static void SetIconWidth(DependencyObject obj, double value)
        {
            obj.SetValue(IconWidthProperty, value);
        }
        /// <summary>
        /// 图标宽度
        /// </summary>
        public static readonly DependencyProperty IconWidthProperty =
            DependencyProperty.RegisterAttached(
                "IconWidth",
                typeof(double),
                typeof(CheckBoxHelper),
                new PropertyMetadata(null));


        public static double GetIconHeight(DependencyObject obj)
        {
            return (double)obj.GetValue(IconHeightProperty);
        }

        public static void SetIconHeight(DependencyObject obj, double value)
        {
            obj.SetValue(IconHeightProperty, value);
        }
        /// <summary>
        /// 图标高度
        /// </summary>
        public static readonly DependencyProperty IconHeightProperty =
            DependencyProperty.RegisterAttached(
                "IconHeight",
                typeof(double),
                typeof(CheckBoxHelper),
                new PropertyMetadata(null));
    }
}
