﻿using Niaofei.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Niaofei.Hello
{
    [Injection(typeof(MenuCommand))]
    internal class HelloCommand : MenuCommand
    {
        public HelloCommand()
        {
            Id ="Hello";
            Name = "Hello";
            OrderCode = "0";
            GroupCode = "0";
        }

        public override void Execute(object? parameter)
        {
            MessageBox.Show("大家好");
        }
    }
}
