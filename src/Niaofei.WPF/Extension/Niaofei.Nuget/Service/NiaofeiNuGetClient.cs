﻿using BaGet.Protocol;
using System.Net;
using System.Net.Http;

namespace Niaofei.Nuget
{
    /// <summary>
    /// Nuget客户端
    /// </summary>
    internal class NiaofeiNuGetClient
    {
        /// <summary>
        /// https://api.nuget.org/v3/index.json
        /// </summary>
        /// <param name="serviceIndexUrl">Nuget服务地址</param>
        public NiaofeiNuGetClient(string serviceIndexUrl)
        {
            var httpClient = new HttpClient(new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
            });

            var clientFactory = new NuGetClientFactory(httpClient, serviceIndexUrl);

            ContentClient = clientFactory.CreatePackageContentClient();
            MetadataClient = clientFactory.CreatePackageMetadataClient();
            SearchClient = clientFactory.CreateSearchClient();
            AutocompleteClient = clientFactory.CreateAutocompleteClient();
        }

        public IPackageContentClient ContentClient { get; private set; }
        public IPackageMetadataClient MetadataClient { get; private set; }
        public ISearchClient SearchClient { get; private set; }
        public IAutocompleteClient AutocompleteClient { get; private set; }
    }
}
