﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Niaofei.Nuget
{
    /// <summary>
    /// NugetItemControl.xaml 的交互逻辑
    /// </summary>
    public partial class NugetItemControl : UserControl
    {
        public NugetItemControl()
        {
            InitializeComponent();
        }

        public BitmapImage Icon
        {
            get { return (BitmapImage)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }
        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(BitmapImage), typeof(NugetItemControl), new PropertyMetadata(null));

        public string NugetName
        {
            get { return (string)GetValue(NugetNameProperty); }
            set { SetValue(NugetNameProperty, value); }
        }
        public static readonly DependencyProperty NugetNameProperty =
            DependencyProperty.Register("NugetName", typeof(string), typeof(NugetItemControl), new PropertyMetadata(""));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(NugetItemControl), new PropertyMetadata(""));
    }
}
