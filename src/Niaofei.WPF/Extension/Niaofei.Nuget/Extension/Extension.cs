﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Nuget
{
    internal static class Extension
    {
        /// <summary>
        /// 集合添加
        /// </summary>
        public static void AddRange<T>(this Collection<T> items, IEnumerable<T> addItems)
        {
            foreach (var item in addItems)
            {
                items.Add(item);
            }
        }
    }
}
