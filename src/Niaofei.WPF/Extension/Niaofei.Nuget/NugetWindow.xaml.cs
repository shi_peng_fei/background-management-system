﻿using System.Windows;

namespace Niaofei.Nuget
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class NugetWindow : Window
    {
        public NugetWindow()
        {
            InitializeComponent();
            DataContext = new NugetWindowViewModel();


        }
    }
}