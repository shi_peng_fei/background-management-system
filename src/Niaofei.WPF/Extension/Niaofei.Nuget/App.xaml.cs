﻿using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Threading;

namespace Niaofei.Nuget
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            this.DispatcherUnhandledException += this.App_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // 处理非 UI 线程中的未处理异常
            var ex = e.ExceptionObject as Exception;
            MessageBox.Show($"发生了未处理的异常: {ex.Message}", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // 处理 UI 线程中的未处理异常
            MessageBox.Show($"发生了未处理的异常: {e.Exception.Message}", "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            // 标记为已处理
            e.Handled = true; 
        }
    }
}
