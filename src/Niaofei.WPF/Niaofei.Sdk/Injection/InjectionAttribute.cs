﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Niaofei.Sdk
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectionAttribute : Attribute
    {
        public InjectionAttribute()
        {

        }

        public InjectionAttribute(Type type)
        {
            Type = type;
            ServiceLifetime = ServiceLifetime.Singleton;
        }

        /// <summary>
        /// 描述
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// 注入的服务
        /// </summary>
        public Type? Type { get; set; }

        /// <summary>
        /// 注入方式
        /// </summary>
        public Func<IServiceProvider, object>? InjectionFunc { get; set; }

        /// <summary>
        /// 注入的生命周期
        /// </summary>
        public ServiceLifetime ServiceLifetime { get; set; }
    }
}
