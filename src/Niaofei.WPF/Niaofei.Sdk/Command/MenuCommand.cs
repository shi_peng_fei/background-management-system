﻿using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Niaofei.Sdk
{
    public class MenuCommand : ICommand
    {
        public MenuCommand()
        {

        }

        public string Id { get; set; } = string.Empty;

        public string? ParentId { get; set; }

        public string? GroupCode { get; set; }

        public string? OrderCode { get; set; }

        public string Name { get; set; } = string.Empty;

        public object? ToolTip { get; set; }

        public BitmapImage? Icon { get; set; }

        public event EventHandler? CanExecuteChanged;

        public virtual bool CanExecute(object? parameter)
        {
            return true;
        }

        public virtual void Execute(object? parameter)
        {

        }

        public virtual bool GetShow()
        {
            return true;
        }

        public virtual bool GetEnabled()
        {
            return true;
        }
    }
}
