## 环境要求

- Node 环境

  版本：16+

- 开发工具

  VSCode

- 必装插件

  - Vue Language Features (Volar)
  - TypeScript Vue Plugin (Volar)

## 项目启动

```bash
# 安装 pnpm
npm install pnpm -g

# 安装依赖
pnpm install

# 项目运行
pnpm run dev

# 项目打包
pnpm run build:prod

```

## 项目部署
* `打包还有点问题，有兴趣的人可以帮忙一下。  打包时候/login路由有问题`
* 可以使用 [脚本](../script/publish.sh)一件docker部署（将整个vue环境打包到docker容器中进行部署，因为打包不成功）。