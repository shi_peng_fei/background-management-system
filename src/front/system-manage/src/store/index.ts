import { defineStore } from 'pinia'
import { ref } from 'vue'

export default defineStore("", () => {
  const count = ref(1);
  const increment = () => {
    count.value++
  }

  return { count, increment }
})