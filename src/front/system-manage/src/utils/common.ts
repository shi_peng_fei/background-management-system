import { ElMessageBox, ElMessage } from 'element-plus'
import { Ref } from 'vue';
import type { FormInstance } from 'element-plus'


/**
 * 
 * @param message 提示信息
 * @param determine 确认后执行的回调
 * @param cancel 取消后执行的回调
 */
export function ConfirmDialog(message: string = "", determine: () => void = () => { }, cancel: () => void = () => { }) {
  ElMessageBox.confirm(
    message,
    'Warning',
    {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning',
    }
  )
    .then(determine)
    .catch(cancel)
}

export interface IPageInput {
  /**
   * 分页长度
   */
  pageSize: number;
  /**
   * 页码
   */
  pageNo: number;
}

export interface IPageOutput<TList> {
  /**
   * 总数
   */
  count: number;
  /**
   * 列表
   */
  list: TList[];
}

export interface IDefaultDto {
  createTime?: string;

  updateTime?: string;

  userId?: string
}

/**
 * 代理类转普通类
 * @param obj 
 * @returns 
 */
export function proxyToObject(obj: any): any {
  if (typeof obj !== 'object' || obj === null) {
    return obj;
  }

  if (Array.isArray(obj)) {
    return obj.map(proxyToObject);
  }

  const plainObj: any = {};
  const keys = Object.keys(obj);

  for (let key of keys) {
    // 如果属性值是代理对象，则递归转换为普通对象
    if (obj[key] instanceof Proxy) {
      plainObj[key] = proxyToObject(obj[key]);
    } else {
      plainObj[key] = obj[key];
    }
  }

  return plainObj;
}

/**
 * DateTime转时间
 */
export function formatDate(date: Date, format: string): string {
  const year = date.getFullYear().toString();
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const day = date.getDate().toString().padStart(2, '0');
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  const seconds = date.getSeconds().toString().padStart(2, '0');

  const formattedDate = format
    .replace('yyyy', year)
    .replace('MM', month)
    .replace('dd', day)
    .replace('HH', hours)
    .replace('mm', minutes)
    .replace('ss', seconds);

  return formattedDate;
}

/**
 * el-tree-select对应数据结构定义
 */
export interface ISelectTreeNode {
  value: number,
  label: string,
  children: ISelectTreeNode[],
}

/**
 * 表单验证方法
 * @param formRef From表单示例
 * @returns 
 */
export function validateForm(formRef: Ref<FormInstance | null>): Promise<void> {
  return new Promise((resolve, reject) => {
    formRef.value?.validate((valid) => {
      if (valid) {
        resolve();
      } else {
        reject(new Error('表单验证失败'));
      }
    });
  });
}