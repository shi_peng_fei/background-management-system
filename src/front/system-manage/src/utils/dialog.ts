export class Dialog {
  constructor() {
    this.dialogVisible = false;
    this.title = ""
  }

  /**
   * 是否现实
   */
  public dialogVisible: boolean;
  /**
   * 弹窗标题
   */
  title: string;

  close(): void {
    this.dialogVisible = false;
  }
  show(title?: string): void {
    this.dialogVisible = true;
    this.title = title ?? "";
  }
}