import { AxiosPromise } from "axios";
import request from '@/utils/request';

export function uploadFile(file: File): AxiosPromise<string> {
  const data = new FormData();
  data.append("file", file)
  return request({
    url: "api/file-manage/upload-file",
    data: data,
    method: "post",
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

export function getFile(path: string): AxiosPromise<File> {
  return request({
    url: "api/file-manage/get-file/" + path,
    method: "get",
  })
}

export function getFileUrl(path: string): string {
  return import.meta.env.VUE_APP_API_URL + "/api/file-manage/get-file/" + path
}