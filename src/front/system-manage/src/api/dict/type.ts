import { IPageInput, IDefaultDto } from "@/utils/common";
import { reactive } from "vue";

export interface IDictType {
    /**
     * Id
     */
    id: number;
    /**
     * 字典类型编码
     */
    code: string;

    /**
     * 字典类型名称
     */
    name: string;

    /**
     * 字典类型状态 0-正常。1-禁用
     */
    status: number;
    /**
     * 备注
     */
    remark: string;
}

export interface IDictTypeInput extends IPageInput {
    /**
     * Id
     */
    id?: number;
    /**
     * 字典类型编码
     */
    code?: string;
    /**
     * 字典类型名称
     */
    name?: string;
}

// 获取必填项校验
export function getRules() {
    return reactive({
        name: [{ required: true, message: '请输入字典类型名称', trigger: 'blur' }],
        code: [{ required: true, message: '请输入字典类型编码', trigger: 'blur' }],
        status: [{ required: true, message: '请输入字典类型状态', trigger: 'blur' }],
    })
}

export interface IDict extends IDefaultDto {
    id?: number;
    /**
     * 数据字典类型Id
     */
    sysDictTypeId?: number;
    /**
     * 数据字典编码
     */
    typeCode?: string;
    /**
     * 数据字典名称
     */
    name?: string;
    /**
     * 数据字典值
     */
    value?: string;
    /**
     * 数据字典排序
     */
    sort: number;
    /**
     * 状态
     */
    status: number;
    /**
     * 是否默认
     */
    isDefault?: boolean;
    /**
     * 备注
     */
    remark?: string;


}

export interface IDictInput extends IPageInput {
    /**
     * 字典类型Id
     */
    sysDictTypeId?: number;
    /**
     * 关键字
     */
    keyWord?: string;
    /**
     * 状态
     */
    status?: number;
    /**
     * 是否默认
     */
    isDefault?: boolean;
}

// 获取必填项校验
export function getDictRules() {
    return reactive({
        name: [{ required: true, message: '请输入字典类型名称', trigger: 'blur' }],
        typeCode: [{ required: true, message: '请输入字典类型编码', trigger: 'blur' }],
        status: [{ required: true, message: '请输入字典状态', trigger: 'blur' }],
        isDefault: [{ required: true, message: '请输入是否默认状态', trigger: 'blur' }],
    })
}