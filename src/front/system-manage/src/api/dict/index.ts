import { AxiosPromise } from "axios";
import request from '@/utils/request';
import { IDictTypeInput, IDictType, IDict, IDictInput } from "@/api/dict/type"
import { IPageOutput } from "@/utils/common";

/**
 * 获取数据字典类型列表
 */
export function getDictTypePage(input: IDictTypeInput): AxiosPromise<IPageOutput<IDictType>> {
    return request({
        url: "api/sys-dict/get-dict-type-page",
        method: "get",
        params: input,
    });
}

/**
 * 删除字典类型
 * @param ids id集合
 * @returns 
 */
export function removeDictType(ids: number[]): AxiosPromise<IDictType> {
    return request({
        url: "api/sys-dict/remove-dict-type",
        method: "delete",
        data: ids,
    });
}

/**
 * 添加数据字典类型
 * @param dict 
 * @returns 
 */
export function addDictType(dict: IDictType): AxiosPromise<IDictType> {
    return request({
        url: "api/sys-dict/add-dict-type",
        method: "post",
        data: dict,
    });
}

/**
 * 更新数据字典类型
 * @param dict 
 * @returns 
 */
export function updateDictType(dict: IDictType): AxiosPromise<IDictType> {
    return request({
        url: "api/sys-dict/update-dict-type",
        method: "put",
        data: dict,
    });
}

// 获取数据字典
export function getDictPage(input: IDictInput): AxiosPromise<IPageOutput<IDict>> {
    return request({
        url: "api/sys-dict/get-dict-page",
        method: "get",
        params: input,
    });
}

// 添加数据字典项
export function addDict(dict: IDict): AxiosPromise<IDict> {
    return request({
        url: "api/sys-dict/add-dict",
        method: "post",
        data: dict,
    });
}

// 修改数据字典项
export function updateDict(dict: IDict): AxiosPromise<IDict> {
    return request({
        url: "api/sys-dict/update-dict",
        method: "put",
        data: dict,
    });
}

// 删除数据字典项
export function deleteDict(ids: number[]): AxiosPromise {
    return request({
        url: "api/sys-dict/remove-dict",
        method: "delete",
        data: ids,
    })
}