import { AxiosPromise } from "axios";
import request from '@/utils/request';
import { ILogin } from "@/api/auth/type";

/**
 * 登录
 * @param login 
 * @returns 
 */
export function login(login: ILogin): AxiosPromise<string> {
  return request({
    url: "api/sys-user/login",
    method: "post",
    params: login
  })
}
