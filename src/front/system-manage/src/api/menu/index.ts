import { IMenu, IMenuInput } from "./type";
import { AxiosPromise } from "axios";
import request from '@/utils/request';

export function getSysMenu(input: IMenuInput): AxiosPromise<IMenu[]> {
  return request({
    url: "api/sys-menu/get-tree-nodes",
    method: "get",
    params: input
  });
}

export function updateSysMenu(menu: IMenu): AxiosPromise<IMenu> {
  return request({
    url: "api/sys-menu/update",
    method: "put",
    data: menu,
  });
}

export function addSysMenu(menu: IMenu): AxiosPromise<IMenu> {
  return request({
    url: "api/sys-menu/add",
    method: "post",
    data: menu,
  });
}

export function deleteSysMenu(menuIds: number[]): AxiosPromise {
  return request({
    url: "api/sys-menu/remove",
    method: "delete",
    data: menuIds,
  });
}

export function getMenuPath(id: Number): AxiosPromise<IMenu[]> {
  return request({
    url: "api/sys-menu/get-menu-path",
    method: "get",
    params: { id },
  })
}