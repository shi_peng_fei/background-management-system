import { reactive } from 'vue';
import { ISelectTreeNode } from "@/utils/common"

export interface IMenu {
    /**
   * 子菜单
   */
    children?: IMenu[];
    /**
     * 组件路径
     */
    component?: string;
    /**
     * ICON
     */
    icon?: string;
    /**
     * 菜单ID
     */
    id?: number;
    /**
     * 菜单名称
     */
    name?: string;
    /**
     * 父菜单ID
     */
    parentId?: number;
    /**
     * 跳转路径
     */
    redirect?: string;
    /**
     * 路由名称
     */
    routeName?: string;
    /**
     * 路由相对路径
     */
    routePath?: string;
    /**
     * 菜单排序(数字越小排名越靠前)
     */
    sort?: number;
    /**
     * 菜单是否可见(1:显示;0:隐藏)
     */
    visible?: number;
    /**
     * 外链接路径
     */
    redirectUrl?: string;
    /**
     * 外链地址
     */
    path?: string;
    /**
     * 栏目类型
     */
    type: number;
}

export interface IMenuInput {
    /**
     * 是否显示隐藏项
     */
    isVisible: boolean;
}

/**
 * 菜单管理数据绑定结构
 */
export class SysMenuModel {
    searchText: string;
    editTitle: string;
    dialogTableVisible: boolean;
    /**
     * @param searchText 搜素文本
     * @param editTitle 操作标题
     * @param dialogTableVisible 是否显示
     */
    constructor(searchText: string = "", editTitle: string = "", dialogTableVisible: boolean = false) {
        this.searchText = searchText;
        this.editTitle = editTitle;
        this.dialogTableVisible = dialogTableVisible;
    }
}

/**
 * menus转el-tree-select对应数据结构
 * @param menus sysmenu列表
 * @returns 
 */
export function menusConvert(menus: IMenu[]): ISelectTreeNode[] {
    let result = [] as ISelectTreeNode[];
    for (const menu of menus) {
        let node = {
            value: menu.id,
            label: menu.name,
            children: [] as ISelectTreeNode[],
        } as ISelectTreeNode;

        if (menu.children != null) {
            node.children = menusConvert(menu.children);
        }
        result.push(node);
    }
    return result;
}

// 获取必填项校验
export function getRules() {
    return reactive({
        name: [{ required: true, message: '请输入菜单名称', trigger: 'blur' }],
        type: [{ required: true, message: '请选择菜单类型', trigger: 'blur' }],
        path: [{ required: true, message: '请输入路由路径', trigger: 'blur' }],
        component: [
            { required: true, message: '请输入组件完整路径', trigger: 'blur' }
        ]
    })
}