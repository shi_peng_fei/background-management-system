import { IDept, IDeptInput, IDeptTree } from "@/api/dept/type"
import { AxiosPromise } from "axios";
import request from '@/utils/request';
import { ISelectTreeNode } from "@/utils/common"

/**
 * 部门添加
 */
export function addSysDept(input: IDept): AxiosPromise<IDept> {
    return request({
        url: "api/sys-dept/add-sys-dept",
        method: "post",
        data: input,
    });
}
/**
 * 获取部门
 */
export function getSysDept(input: IDeptInput): AxiosPromise<IDeptTree[]> {
    return request({
        url: "api/sys-dept/get-sys-dept",
        method: "get",
        params: input,
    });
}

/**
 * 更新部门
 */
export function updateDept(dict: IDept): AxiosPromise<IDept> {
    return request({
        url: "api/sys-dept/update-sys-dept",
        method: "put",
        data: dict,
    });
}

/**
 * 删除部门
 */
export function deleteSysDept(ids: number[]): AxiosPromise {
    return request({
        url: "api/sys-dept/delete-sys-dept",
        method: "delete",
        data: ids,
    })
}


/**
 * depts转el-tree-select对应数据结构
 * @param depts sysmenu列表
 * @returns 
 */
export function deptsConvert(depts: IDeptTree[]): ISelectTreeNode[] {
    let result = [] as ISelectTreeNode[];
    for (const menu of depts) {
        let node = {
            value: menu.id,
            label: menu.name,
            children: [] as ISelectTreeNode[],
        } as ISelectTreeNode;

        if (menu.children != null) {
            node.children = deptsConvert(menu.children);
        }
        result.push(node);
    }
    return result;
}