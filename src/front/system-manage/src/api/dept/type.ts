import { reactive } from 'vue';

export interface IDeptTree {
    /**
     * id
     */
    id?: number;
    /**
     * 父级节点
     */
    parentId?: number;
    /**
     * 部门名称
     */
    name?: string
    /**
     * 部门路径
     */
    treePath?: string;
    /**
     * 排序
     */
    sort: number;
    /**
     * 状态
     */
    status: number;
    /**
     * 子节点
     */
    children: IDeptTree[];
}

export interface IDept {
    /**
        * id
        */
    id?: number;
    /**
     * 父级节点
     */
    parentId?: number;
    /**
     * 部门名称
     */
    name?: string
    /**
     * 部门路径
     */
    treePath?: string;
    /**
     * 排序
     */
    sort?: number;
    /**
     * 状态
     */
    status?: number;
}

export interface IDeptInput {
    /**
     * 关键字
     */
    keyWord?: string;
    /**
     * 状态
     */
    status?: 0;
}

// 获取必填项校验
export function getRules() {
    return reactive({
        name: [{ required: true, message: '请输入部门名称', trigger: 'blur' }],
    })
}