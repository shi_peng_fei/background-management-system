import { IPageInput } from "@/utils/common";
import { reactive } from "vue"

export interface IRole {
    /**
     * 主键
     */
    id?: number;
    /**
     * 角色名称
     */
    name?: string;
    /**
     * 角色编码
     */
    code?: string;
    /**
     * 排序
     */
    sort: number;
    /**
     * 角色状态：正常-1，通用-0
     */
    status: number;
    /**
     * 权限数据
     */
    dataScope?: number;
}

export interface IRoleInputDto extends IPageInput {
    /**
     * 角色名称
     */
    name?: string;
}

// 获取必填项校验
export function getRules() {
    return reactive({
        name: [{ required: true, message: '请输入角色名称', trigger: 'blur' }],
        code: [{ required: true, message: '请输入角色编码', trigger: 'blur' }],
        status: [{ required: true, message: '请输入角色状态', trigger: 'blur' }],
        sort: [{ required: true, message: '请输入角色排序', trigger: 'blur' }],
    })
}