import { IRole, IRoleInputDto } from "./type";
import { IPageOutput } from "@/utils/common"
import { AxiosPromise } from "axios";
import request from '@/utils/request';

/**
 * 用户角色查询
 */
export function getSysRolePage(input: IRoleInputDto): AxiosPromise<IPageOutput<IRole>> {
    return request({
        url: "api/sys-role/get-sys-role-page",
        method: "get",
        params: input,
    })
}

/**
 * 获取用户角色
 */
export function getSysRole(): AxiosPromise<IRole[]> {
    return request({
        url: "api/sys-role/get-sys-role",
        method: "get",
    })
}

/**
 * 删除用户角色
 */
export function deleteSysRole(roleIds: number[]): AxiosPromise {
    return request({
        url: "api/sys-role/delete-sys-role",
        method: "delete",
        data: roleIds,
    });
}

/**
 * 添加用户角色
 */
export function addSysRole(input: IRole): AxiosPromise<IRole> {
    return request({
        url: "api/sys-role/add-sys-role",
        method: "post",
        data: input,
    });
}

/**
 * 更新用户角色
 */
export function updateSysRole(input: IRole): AxiosPromise<IRole> {
    return request({
        url: "api/sys-role/update-sys-user",
        method: "put",
        data: input,
    })
}