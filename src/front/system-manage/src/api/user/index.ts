import { IPageOutput } from "@/utils/common"
import { AxiosPromise } from "axios";
import request from '@/utils/request';
import { IUser, IUserInput } from "@/api/user/type";

export function addUser(user: IUser): AxiosPromise<IUser> {
    return request({
        url: "api/sys-user/add-user",
        method: "post",
        data: user,
    });
}

export function UpdateUser(user: IUser): AxiosPromise<IUser> {
    return request({
        url: "api/sys-user/update-user",
        method: "put",
        data: user,
    });
}

export function getUserPage(input: IUserInput): AxiosPromise<IPageOutput<IUser>> {
    return request({
        url: "api/sys-user/get-user-page",
        method: "get",
        params: input,
    })
}

export function getUser(id: number): AxiosPromise<IUser> {
    return request({
        url: "api/sys-user/get-user",
        method: "get",
        params: { id }
    });
}

export function getCurrentUser(): AxiosPromise<IUser> {
    return request({
        url: "api/sys-user/get-current-user",
        method: "get",
    });
}

export function deleteUser(id: number): AxiosPromise<IUser> {
    return request({
        url: "api/sys-user/remove-user",
        method: "delete",
        params: { id },
    });
}


export function getUserAvatar(userName: string): AxiosPromise<string> {
    return request({
        url: "api/sys-user/get-user-avatar",
        method: "get",
        params: { userName },
    })
}