import { IPageInput } from "@/utils/common"
import { reactive } from "vue"

export interface IUserBase {
    /**
     * 用户名
     */
    userName?: string;
    /**
     * 用户昵称
     */
    nickName?: string;
    /**
     * 性别
     */
    gender?: number;
    /**
     * 年龄
     */
    age?: number;
    /**
     * 密码
     */
    passWord?: string;
    /**
     * 部门Id
     */
    deptId?: number;
    /**
     * 头像
     */
    avatar?: string;
    /**
     * 联系方式
     */
    mobile?: string;
    /**
     * 邮箱
     */
    email?: string;
    /**
     * 状态
     */
    status?: number;

    /**
     * 角色
     */
    roles?: number[];
}

export interface IUser extends IUserBase {
    /**
        * 主键
        */
    id?: number;

    /**
     * 部门名称
     */
    deptName?: string;
}

export interface IUserInput extends IPageInput {
    /**
     * 关键字
     */
    KeyWord?: string,
    /**
     * 部门名称
     */
    deptId?: number,
}

// 获取必填项校验
export function getRules() {
    return reactive({
        userName: [{ required: true, message: '请输入用户名称', trigger: 'blur' }],
        nickName: [{ required: true, message: '请输入用户昵称', trigger: 'blur' }],
        deptId: [{ required: true, message: '请输入所属部门', trigger: 'blur' }],
        gender: [{ required: true, message: '请输入性别', trigger: 'blur' }],
        mobile: [{ required: true, message: '请输入手机号', trigger: 'blur' }],
        passWord: [{ required: true, message: '请输入密码', trigger: 'blur' }],
        status: [{ required: true, message: '请输入密码', trigger: 'blur' }],
    })
}