import {
  createRouter,
  createMemoryHistory,
  createWebHashHistory,
  createRouterMatcher,
  RouteRecordRaw,
  createWebHistory
} from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/sysDictManage"
    //component: () => import("@/views/sysView/sysMenuManage.vue")
  },
  {
    path: "/login",
    component: () => import("@/views/login.vue")
  },
  {
    path: "/back",
    component: () => import("@/layout/index.vue"),
    children: [
      {
        path: "/sysDictManage",
        component: () => import("@/views/sysView/sysDictManage.vue")
      },
      {
        path: "/sysMenuManage",
        component: () => import("@/views/sysView/sysMenuManage.vue")
      },
      {
        path: "/sysRoleManage",
        component: () => import("@/views/sysView/sysRoleManage.vue"),
      },
      {
        path: "/sysDeptManage",
        component: () => import("@/views/sysView/sysDeptManage.vue"),
      },
      {
        path: "/sysUserManage",
        component: () => import("@/views/sysView/sysUserManage.vue"),
      },
      {
        path: "/sysRights",
        component: () => import("@/views/sysView/sysRights.vue"),
      },
      {
        path: "/tow",
        component: () => import("@/views/tow.vue")
      },
      {
        path: "/index",
        component: () => import("@/views/index.vue"),
      }
    ]
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes
})

export function resetRouter() {
  router.push({ path: '/login' });
  // location.reload();
}

export default router
