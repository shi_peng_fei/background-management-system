import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path';
const pathSrc = path.resolve(__dirname, 'src');

export default defineConfig(({ command, mode }) => {
  // 根据当前工作目录中的 `mode` 加载 .env 文件
  // 设置第三个参数为 '' 来加载所有环境变量，而不管是否有 `VITE_` 前缀。
  const env = loadEnv(mode, process.cwd(), '')
  // console.log(env)
  return {
    build: {
      outDir: 'dist',
      emptyOutDir: true,
      assetsInlineLimit: 0,
      rollupOptions: {
        output: {
          manualChunks: undefined
        }
      }
    },
    server: {
      host: '0.0.0.0',
      port: 5000,
      strictPort: true,
      fs: {
        strict: false
      }
    },
    plugins: [vue()],
    resolve: {
      alias: {
        '@': pathSrc
      }
    },
    define: {
      'import.meta.env.VUE_APP_API_URL': JSON.stringify(env.VUE_APP_API_URL)
    },
  }
})
