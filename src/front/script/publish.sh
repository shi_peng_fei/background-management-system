#!/bin/bash

echo "进入到对应的路径"
cd ../system-manage
echo "目前的路径"
pwd

echo "停止容器"
docker stop niaofeipage

echo "删除容器"
docker rm niaofeipage

echo "删除镜像"
docker rmi niaofeipage:1.0

docker build -t niaofeipage:1.0 .

docker run -d --name=niaofeipage -p 5000:5000 niaofeipage:1.0

echo "前端发布成功"