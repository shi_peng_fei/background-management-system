# 后台管理系统

#### 介绍
基于.net6+vue3技术栈实现的后端管理系统
> 前端使用vue、elementPlus等常用前端技术

> 后端使用.netCore Webapi实现，还用到EFCore、Redis、Minio等后端开发常用技术。

#### 安装教程
* 后端拉取代码配置好minio、mysql、redis后可以直接启动。
* 前端使用vite构建，具体参照前端Readme.
#### 功能模块
> 后端：Redis封装、仓储封装、动态依赖注入、Minio、身份认证、日志等常用封装；以及用户管理、数据字典、权限管理、工作流（未完成）等基础模块的封装。

> 前端：实现后台管理系统相关页面：菜单栏、导航栏、窗体、登录等。

#### 产品设计
* 后端设计：[后端Readme](./src/back/Niaofei/Readme.md)
* 前端设计：[前端Readme](./src/front/system-manage/README.md)

#### 作品展示
[鸟飞管理系统](http://niaofei.top:5000)

`点击（去注册）直接登录`