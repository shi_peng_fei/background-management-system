### 下拉树绑定数据源

如果你的数据源中有一个唯一的 `id` 字段，并且你想要将用户选择的项的 `id` 绑定到 `v-model` 属性上，你可以使用以下方法：

```html
<el-tree-select v-model="selectedItemId" :data="treeData" :props="defaultProps"></el-tree-select>
```

在你的 Vue 实例中，你需要定义 `selectedItemId` 和 `defaultProps`，如下所示：

```javascript
data() {
  return {
    treeData: [{
      "id": 1,
      "name": "鸟飞部门",
      "parentId": null,
      "treePath": "niaofei",
      "sort": 0,
      "status": 0,
      "children": []
    }],
    defaultProps: {
      children: 'children',
      label: 'name',
      value: 'id'
    },
    selectedItemId: null
  };
}
```

在上面的示例中，我们将 `defaultProps` 中的 `value` 属性设置为 `'id'`，这样 `el-tree-select` 组件就会根据 `id` 字段来标识每个选项。同时，我们定义了 `selectedItemId` 变量来存储用户选择的项的 `id` 值。

希望这能帮到你！