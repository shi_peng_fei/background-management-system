当 HTML 页面中的 input 元素获取焦点后，按下回车键会触发表单的默认提交行为。这通常会导致页面的刷新或重新加载。

如果你希望按下回车键不引起页面刷新，可以采取以下几种方法：

1. 阻止表单的默认提交行为：
   在表单的 onsubmit 事件中添加事件处理程序，并使用 `event.preventDefault()` 方法来阻止表单的默认提交行为。例如：

   ```html
   <form onsubmit="event.preventDefault()">
     <!-- 表单内容 -->
   </form>
   ```

   这样，当按下回车键时，表单将不再执行默认的提交操作，页面也不会刷新。

2. 将 input 元素的 type 属性设置为 "button"：
   将 input 元素的 type 属性设置为 "button"，而不是默认的 "submit"。这样按下回车键时，不会触发表单的提交行为。例如：

   ```html
   <input type="button" value="Submit">
   ```

   这样，按下回车键时，input 元素不会导致页面刷新。

3. 使用 JavaScript 监听键盘事件：
   可以使用 JavaScript 添加键盘事件监听器，在按下回车键时执行自定义操作，而不是表单的默认提交行为。例如：

   ```html
   <input type="text" id="myInput">
   ```

   ```javascript
   const myInput = document.getElementById("myInput");
   myInput.addEventListener("keydown", function(event) {
     if (event.key === "Enter") {
       event.preventDefault(); // 阻止默认提交行为
       // 执行自定义操作
       // ...
     }
   });
   ```

   上述示例中，当按下回车键时，阻止了默认的提交行为，并可以在事件处理程序中执行自定义操作。

通过以上方法，你可以根据具体需求，在按下回车键时控制是否刷新页面或执行其他操作。

希望对你有所帮助！