使用 C# 自带的 `System.Text.Json` 库实现将 JSON 字符串转换为 JSON 对象。以下是一个示例代码：

```csharp
using System.Web.Http;
using System.Text.Json;

namespace YourNamespace.Controllers
{
    public class YourController : ApiController
    {
        [HttpPost]
        public IHttpActionResult PostJsonString([FromBody] string jsonString)
        {
            try
            {
                // 使用 System.Text.Json 库将 JSON 字符串解析为 JsonDocument 对象
                var jsonDocument = JsonDocument.Parse(jsonString);

                // 将 JsonDocument 转换为动态对象
                dynamic jsonObject = JsonObjectToDynamic(jsonDocument.RootElement);

                // 在这里可以对 jsonObject 进行处理，例如提取属性值等操作

                // 返回 JSON 对象
                return Ok(jsonObject);
            }
            catch
            {
                // JSON 解析失败，返回错误信息
                return BadRequest("Invalid JSON format");
            }
        }

        private static dynamic JsonObjectToDynamic(JsonElement element)
        {
            // 使用递归将 JsonElement 转换为动态对象
            switch (element.ValueKind)
            {
                case JsonValueKind.Object:
                    var obj = new ExpandoObject();
                    foreach (var property in element.EnumerateObject())
                    {
                        ((IDictionary<string, object>)obj)[property.Name] = JsonObjectToDynamic(property.Value);
                    }
                    return obj;

                case JsonValueKind.Array:
                    var array = new List<object>();
                    foreach (var item in element.EnumerateArray())
                    {
                        array.Add(JsonObjectToDynamic(item));
                    }
                    return array;

                case JsonValueKind.String:
                    return element.GetString();

                case JsonValueKind.Number:
                    // 根据实际情况，可将数值转换为具体的数值类型
                    return element.GetDecimal();

                case JsonValueKind.True:
                    return true;

                case JsonValueKind.False:
                    return false;

                default:
                    return null;
            }
        }
    }
}
```

在上面的示例中，我们使用 `System.Text.Json` 库的 `JsonDocument` 类将 JSON 字符串解析为 `JsonDocument` 对象。

然后，通过递归遍历 `JsonElement` 对象的属性和数组元素，并使用动态对象 `ExpandoObject` 构建一个动态对象。根据属性的值类型，我们可以将其转换为相应的动态类型（例如字符串、数值、布尔值等）。

最后，返回转换后的动态对象作为响应给客户端。

这种方法不需要使用 `JObject`，而是使用了 C# 自带的 `System.Text.Json` 库来实现 JSON 解析和转换。希望对你有所帮助！