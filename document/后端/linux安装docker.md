在 CentOS 上安装 Docker 通常分为以下几个步骤：

1. **卸载旧版本（可选）**：如果你之前安装过旧版本的 Docker，可以先卸载它们。

2. **添加 Docker YUM 仓库**：向系统添加 Docker YUM 仓库地址，这样系统就能够从该仓库中获取 Docker 安装包。

3. **安装 Docker CE**：使用 YUM 包管理器安装 Docker CE（社区版）。

4. **启动 Docker 服务**：安装完成后，启动 Docker 服务，并设置开机自启动。

下面是详细步骤：

### 1. 卸载旧版本（可选）

如果你之前安装过旧版本的 Docker，可以使用以下命令进行卸载：

```bash
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### 2. 添加 Docker YUM 仓库

执行以下命令将 Docker YUM 仓库地址添加到系统中：

```bash
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

### 3. 安装 Docker CE

安装 Docker CE：

```bash
sudo yum install docker-ce docker-ce-cli containerd.io
```

### 4. 启动 Docker 服务

安装完成后，启动 Docker 服务：

```bash
sudo systemctl start docker
```

设置 Docker 开机自启动：

```bash
sudo systemctl enable docker
```

### 5. 验证安装

运行以下命令，检查 Docker 是否已成功安装并正常运行：

```bash
sudo docker --version
```

以上是在 CentOS 上安装 Docker CE 的基本步骤。安装完成后，你就可以使用 Docker 来构建、部署和运行容器化应用程序了。