[多次加载同一个dll](https://stackoverflow.com/questions/1528287/assembly-gettype-and-typeof-return-different-types)

**不使用加载dll的方式获取系统中全部程序集**

在 .NET Framework 和 .NET Core 中，你可以使用 `Assembly.GetReferencedAssemblies` 方法来获取程序所依赖的全部程序集（Assembly）。这个方法可用于获取当前应用程序域中加载的程序集以及它们引用的其他程序集。

以下是一个示例代码，演示如何获取程序所依赖的全部程序集：

```csharp
using System;
using System.Reflection;

public class Program
{
    public static void Main(string[] args)
    {
        // 获取当前应用程序域中加载的所有程序集
        Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

        // 遍历每个程序集的引用程序集
        foreach (Assembly assembly in assemblies)
        {
            // 获取引用的程序集信息
            AssemblyName[] referencedAssemblies = assembly.GetReferencedAssemblies();

            // 打印引用的程序集名字
            foreach (AssemblyName referencedAssembly in referencedAssemblies)
            {
                Console.WriteLine(referencedAssembly.FullName);
            }
        }
    }
}
```

在上述示例中，我们使用 `AppDomain.CurrentDomain.GetAssemblies()` 方法获取当前应用程序域中加载的所有程序集，并将其存储在 `assemblies` 数组中。然后，我们遍历每个程序集，使用 `assembly.GetReferencedAssemblies()` 方法获取该程序集引用的其他程序集的信息，并将其存储在 `referencedAssemblies` 数组中。最后，我们遍历每个引用程序集，打印它们的全名（FullName）。

运行该示例代码，你将获得当前应用程序所依赖的全部程序集的全名。请注意，这将包括从运行时加载的程序集以及该程序集引用的其他程序集。