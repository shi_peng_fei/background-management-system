要在 C# Web API 中全局设置接口的 `DateTime` 类型的 JSON 序列化格式为 "yyyy-MM-dd HH:mm:ss"，你可以通过以下步骤实现：

1. 在你的 Web API 项目中，找到 `Startup.cs` 文件。

2. 在 `ConfigureServices` 方法中，找到并注释或删除掉默认的 JSON 序列化配置，通常是以下代码：

```csharp
services.AddControllers();
```

3. 替换为以下代码：

```csharp
services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
});
```

4. 在同一个文件中，添加一个自定义的 `DateTimeConverter` 类。这个类将继承 `JsonConverter<DateTime>` 并覆写 `Read` 和 `Write` 方法，如下所示：

```csharp
public class DateTimeConverter : JsonConverter<DateTime>
{
    private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return DateTime.ParseExact(reader.GetString(), DateTimeFormat, CultureInfo.InvariantCulture);
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString(DateTimeFormat));
    }
}
```

上述代码中，`Read` 方法将把字符串解析成 `DateTime` 对象，而 `Write` 方法将把 `DateTime` 对象转换成指定的字符串格式。

5. 保存文件并重新启动 Web API 项目。现在，在所有返回 `DateTime` 类型的 JSON 字段中，日期时间将以 "yyyy-MM-dd HH:mm:ss" 的格式进行序列化。

请注意，这种全局设置仅适用于 Web API 项目中的 `DateTime` 类型，而不会影响其他部分或其他类型的序列化。