# C#后端开发请求相关知识
## C# WebApi接收参数的常用方式
* 查询字符串参数：查询字符串是通过 HTTP GET 请求中的 URL 中传递的键值对。在 WebAPI 中，可以使用以下方式获取查询字符串参数：
```csharp
// 使用 Request.QueryString 获取单个查询字符串参数
string name = Request.QueryString["name"];

// 使用 Request.QueryString 获取所有查询字符串参数
IEnumerable<string> queryKeys = Request.Query.Keys;
foreach (var key in queryKeys)
{
    string value = Request.Query[key];
}
```
* 路由参数：路由参数是从 URL 中提取的变量值。在 WebAPI 中，可以使用以下方式获取路由参数：
```csharp
[HttpGet("users/{id}")]
public IActionResult GetUser(int id)
{
    // 处理路由参数 id
}
```
* 请求正文：请求正文通常用于在 HTTP POST 或 PUT 请求中传递复杂的数据类型，例如 JSON 或 XML。在 WebAPI 中，可以使用以下方式获取请求正文：
```csharp
[HttpPost("users")]
public IActionResult CreateUser([FromForm] User user)
{
    // 处理表单数据 user
}
```
* 头部信息：HTTP 头部信息通常包含客户端和服务器之间的元数据。在 WebAPI 中，可以使用以下方式获取头部信息：
``` csharp
// 使用 Request.Headers 获取所有头部信息
IEnumerable<string> headerKeys = Request.Headers.Keys;
foreach (var key in headerKeys)
{
    string value = Request.Headers[key];
}

// 使用 Request.Headers 获取单个头部信息
string contentType = Request.Headers["Content-Type"]
```

**总结：在请求过程中，请求的信息可以放置在3个地方**
1. 放置在请求路径中，有`QueryString`查询字符串和`Router`路由参数两种方式。
2. 放置在请求头`Header`中。
3. 放置在请求体`Body`中:
   1. 在请求头中设置*Context-Type=application/json*等文本类型时,C#后端会把Body中的数据当中Json字符串处理，用`[FromBody]`进行参数接收。
   2. 在请求头中设置*Context-Type=multipart/form-data*类型时候，C#后端会把Body中的数据当作键值对来进行解析，用`[FromFrom]`进行参数接收。