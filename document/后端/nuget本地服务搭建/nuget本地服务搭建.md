## 本地Nuget服务搭建方式

### Nuget.Server

> 这是一种比较老的方式，支支持Framework运行环境，并且对于nuget v3版本以上的api接口不支持，同时只能部署在windows服务器上面，没有管理界面

**部署方式**

1.创建一个asp.net（.NET framework）程序。

![](./images/新建ASP.net程序.png)



![](./images/asp.net版本.png)

![](./images/asp.net空.png)



2. 添加Nuget.Server引用。

![](./images/nuget1.png)

![](./images/nuget2.png)



3. 安装好后会自动添加一些代码文件和配置项，这里保持默认就行，我们只需要关注Nuget的几个配置项就行。

![](./images/config1.png)



4. 将nuget密钥取消，将requireApiKey改为false。

![](./images/config2.png)

5. 将Nuget.Server项目发布，得到我们需要的Nuget.Server程序。

![](./images/publish1.png)



![](./images/publish2.png)

![](./images/publish3.png)

![](./images/publish4.png)



4. 将程序部署到IIS服务器。

![](./images/publish5.png)

5. 使用Nuget本地服务

![](./images/use1.png)

![](./images/use2.png)

![](./images/use3.png)

![](./images/use4.png)

6. 使用命令上传nuget包。

```
dotnet nuget push minio.6.0.2.nupkg --source http://192.168.0.102:7000

注释： dotnet nuget publish [包的路径] --source [nuget地址]
```

![](./images/upload1.png)

### Baget

























