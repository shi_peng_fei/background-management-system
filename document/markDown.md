# MarkDown教程
```
1. 标题 # 加标题内容
2. 段落 - 加上段落内容
3. 超链接 [链接名称](链接地址)
4. 图片  ![图片名](图片地址)
5. 引用  >
6. 斜体  *斜体内容*
7. 粗体  **粗体内容**
8. 斜体加粗  ***斜体加粗***
9. 特殊标记 `特殊标记内容`
10. 代码块 
```

```c#
var a = 1;
```

## 标题示例
 - 段落1
 - 段落2
    - 二级段落

[百度](http://www.baidu.com)

![图片]()

> 引用

*斜体*

**加粗**

***斜体加粗***

---
 分割线

 `特殊标记`

 [详细教程](https://juejin.cn/post/7254107670012510245?searchId=20231122212650F147175FF61BB55A9FEA)

# Mermaid语法
## 流程图

```mermaid
flowchart LR
    markdown["`This **is** _Markdown_`"]
    newLines["`Line1
    Line 2
    Line 3`"]
    End["结束"]
    markdown --> newLines --> End
```

## 序列图

```mermaid
sequenceDiagram
    Alice->>John: Hello John, how are you?
    John-->>Alice: Great!
    Alice-)John: See you later!
```


## 类图

```mermaid
classDiagram
    note "From Duck till Zebra"
    Animal <|-- Duck
    note for Duck "can fly\ncan swim\ncan dive\ncan help in debugging"
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
        +String beakColor
        +swim()
        +quack()
    }
    class Fish{
        -int sizeInFeet
        -canEat()
    }
    class Zebra{
        +bool is_wild
        +run()
    }
```
## 状态图
```mermaid
stateDiagram-v2
    [*] --> Still
    Still --> [*]

    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```

## 实体关系图

```mermaid
erDiagram
    CUSTOMER ||--o{ ORDER : places
    CUSTOMER {
        string name
        string custNumber
        string sector
    }
    ORDER ||--|{ LINE-ITEM : contains
    ORDER {
        int orderNumber
        string deliveryAddress
    }
    LINE-ITEM {
        string productCode
        int quantity
        float pricePerUnit
    }
```

## 用户旅程图
```mermaid
journey
    title My working day
    section Go to work
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 5: Me
```

## 思维导图
```mermaid
mindmap
  root((mindmap))
    Origins
      Long history
      
    Research
      On Automatic creation
        Uses
        Creative techniques
```

[具体教程](https://mermaid.nodejs.cn/syntax/flowchart.html)